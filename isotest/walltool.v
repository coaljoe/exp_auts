module main

struct WallTool {

}

fn new_walltool() &WallTool {
	println("new_walltool")

	wt := &WallTool{}

	return wt
}

fn (mut wt WallTool) can_build_wall(start_cx int, start_cy int, end_cx int, end_cy int) bool {
	return true
}

// Build wall from start to end
fn (mut wt WallTool) build_wall(start_cx int, start_cy int, end_cx int, end_cy int) {
	println("WallTool build_wall: start_cx: $start_cx, start_cy: $start_cy, end_cx: $end_cx, end_cy: $end_cy")

	// XXX only 45 degree turns ?

	mut points := []Pos{}

	// XXX
	if end_cx == -1 && end_cy == -1 {
		points << Pos{start_cx, start_cy}
	} else {
		points = diag_line_points(start_cx, start_cy, end_cx, end_cy)
	}
	

	println("")
	println("points: $points")

	//pp(2)

	for point in points {
		println("-> point: $point")

		mut wall := new_wall()

		wall.place_at(point.x, point.y)
	
		wall.spawn()

		// XXX upd sprite
		wall.view.update(0)
	}

	println("done build_wall")
}
