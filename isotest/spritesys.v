module main

interface Drawable {
	draw()
}

fn depth_compare(a &&Sprite, b &&Sprite) int {

	//println(a.y)
	//println(b.y)

	if a.y < b.y {
		//println("ret -1")
		return -1
	}
	if a.y > b.y {
		//println("ret 1")
		return 1
	}
	//println("ret 0")
	//pp(4)
	return 0
}

struct SpriteSys {
	System // Embed
//mut:
	//sys &System // Embed
	
}

fn new_spritesys() &SpriteSys {
	println("new_spritesys()")

	s := &SpriteSys{
		//sys: new_system("SpriteSys", "Sprite system")
		System: new_system("SpriteSys", "Sprite system")
	}

	return s
}

fn (mut s SpriteSys) add(spr &Sprite) {
	s.add_elem(spr)
}

fn (mut s SpriteSys) remove(spr &Sprite) {
	s.remove_elem(spr)
}

fn (mut s SpriteSys) draw() {
	println("spritesys draw()")

	// XXX TODO sort sprites by z

	//mut sorted_elems := s.sys.elems.clone()
	//sorted_elems.sort_with_compare(depth_compare)

	///*
	mut sorted_sprites := []&Sprite{}
	for el in s.elems {
		mut x := &Sprite(el)
		sorted_sprites << x
	}

	sorted_sprites.sort_with_compare(depth_compare)
	//*/
	//mut sorted_sprites := s.sys.elems

	for mut x in sorted_sprites {
		mut z := x
		_ = z
		z.draw()
		//println(z.y)
	}

	sorted_sprites.clear()
	//free(sorted_sprites)

	//pp(3)

	/*
	for el in s.sys.elems {
		//el.draw()
		//x := Drawable{el}
		//mut x := Drawable(el)
		//mut x := &Drawable(el)
		mut x := &Sprite(el)
		x.draw()
		
		println(x.y)
	}
	*/
}

fn (mut s SpriteSys) update(dt f32) {

}
