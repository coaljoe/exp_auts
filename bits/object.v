module bits

import fixed
import rand

// Object bit
//  size: 10x10x10 (cm) (?)
//
// XXX cannot be compound?
struct ObjectBit {
	elem_type ElemType
}

pub struct Object {
pub mut:
	name string
	
	// Dimentions in bits
	// XXX optional, not used.
	//dim_x int // Approx.
	//dim_y int
	//dim_z int
	// XXX use volume to calculate object bounds.

	// Material/Elem
	//elem &Elem // XXX use id/enum
	mat_info &MaterialInfo
	//parts_info &PartsInfo

	// Position in the world
	// XXX in map cells/coords?
	// XXX approximate, limited precision
	pos_x int
	pos_y int
	pos_z int

	// Flixible position in the world
	//transform &Transform

	// Object size/volume in bits
	// amount of bits
	//bits_size int

	// Bits of object's voxel model
	//bits [10][10]ObjectBit

	id string

	// Object's child nodes
	// XXX fuse this nodes into the main object if
	// they're not needed for editing/modification
	// XXX still leave metadata about them for debug?
	// XXX add object.finalize() method for final fusing?
	children []&Object
}

pub fn new_object() &Object {
	id := rand.uuid_v4()
	println("bits: new_object() id=$id")
	
	o := &Object{
		//elem: &Elem(0),
		//mat_info: &MaterialInfo(0),
		//parts_info: &PartsInfo(0),
		//parts_info: new_parts_info(),
		mat_info: new_material_info(),
		//transform: new_transform(),

		id: id,
	}
	
	return o
}

pub fn (mut o Object) add_child(child &Object) {
	println("Object add_child: child.id: $child.id")

	if o.has_child(child) {
		//return error("already have child")
		panic("already have child")
	}

	o.children << child

	//return none
}

pub fn (o &Object) has_child(child &Object) bool {
	return child in o.children
}

pub fn (mut o Object) remove_child(child &Object) {
	println("Object add_child: child.id: $child.id")

	if !o.has_child(child) {
		//return error("no such child")
		panic("no such child")
	}

	idx := o.children.index(child)

	o.children.delete(idx)

	//return none
}

pub fn (o &Object) num_children() int {
	return o.children.len
}

pub fn (mut o Object) fuse_child(child &Object) {
	println("Object fuse_child: child.id: $child.id")

	// XXX transfer material data to parent

	for ch_me in child.mat_info.elems_list() {
		println("-> ch_me: $ch_me")
	
		// XXX add material info
		o.mat_info.add_elem_from_material_elem(ch_me)

		// XXX add parts_meta
	}

	// Delete child from object
	o.remove_child(child)
}

// XXX fuse children
pub fn (mut o Object) fuse_children() {
	println("Object fuse_children")

	// XXX fixme?
	list_copy := o.children.clone()

	// XXX updates o.children
	//for ch in o.children {
	for ch in list_copy {
		// XXX deletes children from the list
		o.fuse_child(ch)
	}
}

// Calc total volume in bits
pub fn (o &Object) bits_volume() fixed.Fixed {
	//return o.dim_x * o.dim_y * o.dim_z
	//return o.bits_size

	mut ret := fixed.new_f(0.0)
	for me in o.mat_info.elems_list() {
		ret = ret.add(me.bits_size)
	}
	
	return ret
}

// Calc total volume in m3
pub fn (o &Object) volume() f32 {
	return bits_to_m3(o.bits_volume())
}

// Calc total mass recursive with children
// XXX rename to mass_rec() or absoulte_mass()?
pub fn (o &Object) mass_rec() f32 {
	//println("Object mass_rec")

	mut total_mass := o.mass()

	for ch in o.children {
		total_mass += ch.mass()
	}

	return total_mass
}

// Calc total mass in kg
pub fn (o &Object) mass() f32 {
	//println("Object mass")

	mut total_mass := f32(0.0)

	for _, me in o.mat_info.elems {

		//println("-> me: $me.elem.name")

		mat_density := me.elem.density
		//mat_volume := f32(me.bits_size.value())
		mat_volume := bits_to_m3(me.bits_size) // XXX

		// XXX is this correct?
		// shouldn't this be in m3 instead?
		mass := mat_density * mat_volume

		//println("mat_density: $mat_density")
		//println("mat_volume: $mat_volume")
		//println("mass: $mass")

		total_mass += mass
	}

	return total_mass
}

/*
// Set size in meters
fn (mut o Object) set_size(mx, my, mz f32) {
	println("object set_size: mx: $mx, my: $my, mz: $mz")
	
	/*
	scale := f32(100.0 / c_bit_size)
	o.dim_x = int(mx * scale)
	o.dim_y = int(my * scale)
	o.dim_z = int(mz * scale)
	*/

	/*
	o.dim_x = m_to_bits(mx)
	o.dim_y = m_to_bits(my)
	o.dim_z = m_to_bits(mz)
	*/

	vol := mx * my * mz
	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")
	//panic(2)
	//exit(2)

	o.bits_size = vol_bits
}
*/

// Get size in m3
pub fn (o &Object) size() f32 {
	return o.volume()
}

/*
// Get size in meters
fn (o &Object) size() (f32, f32, f32) {
	/*
	scale := f32(c_bit_size / 100.0)
	mx := o.dim_x * scale
	my := o.dim_y * scale
	mz := o.dim_z * scale
	*/
	mx := bits_to_m(o.dim_x)
	my := bits_to_m(o.dim_y)
	mz := bits_to_m(o.dim_z)
	
	return mx, my, mz
}
*/
