module bits

import json
//import os

struct SetLoader {

}

// Elems.json
// Root
struct SetFile {
	name string
	description string [optional]

	notes string [optional]
	
	elems_load []string
}

pub fn new_set_loader() &SetLoader {
	println("bits: new_set_loader()")

	l := &SetLoader{}

	return l
}

pub fn (mut l SetLoader) load(res_path string) {
	println("bits: SetLoader load: res_path: $res_path")

	path := resolve_path(res_path) or {
		panic(err)
	}

	def_path := path + "/def.jsonnet"

	data := load_jsonnet_file_string(def_path)

	println("data:")
	println("$data")

	///*
	js := json.decode(SetFile, data) or {
		println("json decode error:")
		panic(err)
		return
	}
	//*/

	// Load elem lib from set
	mut ell := new_elem_lib_loader()

	println("recs:")
	println(js.elems_load)

	for s in js.elems_load {
		println("loading record '$s'...")
	
		//ell.load(bits_ctx.res_path + "/sets/" + s)
		ell.load("sets/" + s)
	}

	// Finally

	// Load elems from this set
	//ell.load(path + "/elems.json")
	ell.load(bits_ctx.set_path + "/elems.jsonnet")

	println("bits: done SetLoader load")
}
