module bits

import fixed
import math

/*
// Material Elem Record
struct MaterialElem {
mut:
	// n_bits
	bits_size int
	elem &Elem
}
*/


// Many objects can share the same material
// Can be compound / multiple elements per material
// XXX TODO: ? bits amount values / calculations in percentages?
// XXX like 20% wood 80% grass?
// XXX how to calculate the amount of material/bits?
// XXX via object mass/weight (must be specified)?

// XXX or 10% glass 90% concrete?
// XXX do we really need such materials?

//struct ObjectPart {
// XXX MaterialElemPart
// XXX MaterialElemFraction
// XXX MaterialFraction
//struct MaterialElemFraction {
// XXX Material Elem Fraction
struct MaterialElem {
pub mut:
	elem &Elem
	//bits_size int
	bits_size fixed.Fixed
	//distinct bool
}

// Information about a part of material
//struct MaterialPartMeta {
struct MaterialPart {
pub mut:
	name string
	elem &Elem
	//bits_size int
	bits_size fixed.Fixed
	// Mark as distinct. Do not fuse/combine.
	distinct bool
}

// Metainformation about a material part
// XXX meta
// XXX can't have backlinks to MaterialElem Fraction
struct MaterialPartMeta {
pub mut:
	name string
	//elem &Elem // XXX rename to elem_name / elem_uid / elem_id (?)
	elem_name string
	bits_size fixed.Fixed // XXX meta data
	// ?
	distict bool
}

// MaterialInfo information about materials in object

// PartsInfo
//struct PartsInfo {
struct MaterialInfo {
pub mut:
	//elems map[string]&Elem
	//elems map[string]&MaterialElem
	//elems map[string]&MaterialPart
	
	// Accumulation of material parts to material element fractions
	// Unique for element (key=elem.name)
	// Fused materials
	// XXX unnamed parts and named parts (fused)
	//elems map[string]&MaterialElemFraction
	elems map[string]&MaterialElem
	// General / Extra / Meta information about material parts
	// Separate materials
	//parts []&MaterialPart
	// XXX named parts (meta, distinct, separate)
	// XXX rename to meta_parts?
	parts_meta []&MaterialPartMeta
}

pub fn new_material_info() &MaterialInfo {
	mi := &MaterialInfo{}

	return mi
}

/*
// XXX
pub fn (mi &MaterialInfo) calc_elems() map[string]&MaterialElem {
	println("MaterialInfo calc_elems")

	mut elems := map[string]&MaterialElem
	for pt in mi.parts {
		k := pt.elem.name
		if !(k in elems) {
			elems[k] = pt
		} else {
			// Sum elems
			//elems[k].bits_size += pt.bits_size
			elems[k].bits_size.add(pt.bits_size)
		}
	}

	return elems
}

pub fn (mut mi MaterialInfo) update_elems() {
	println("MaterialInfo update_elems")
	
	elems := mi.calc_elems()
	mi.elems = elems
}
*/

pub fn (mi &MaterialInfo) has_elem(elem &Elem) bool {
	return elem.name in mi.elems
}

pub fn (mi &MaterialInfo) has_elem_name(elem_name string) bool {
	return elem_name in mi.elems
}

// Add elem record
// XXX internal?
pub fn (mut mi MaterialInfo) add_elem(name string, bits_size fixed.Fixed, elem &Elem) {
	println("MaterialInfo add_elem: name: '$name', bits_size: $bits_size, elem.name: $elem.name")

	k := elem.name

	if !(k in mi.elems) {
		rec := &MaterialElem{
			elem: elem,
			bits_size: bits_size,
		}	
		mi.elems[k] = rec
	} else {
		// Update/fuse record
		mut rec := mi.elems[k]
		rec.bits_size = rec.bits_size.add(bits_size)
	}

	// Record named material part metadata
	// XXX
	if name != "" {
		//pp(2)

		rec := &MaterialPartMeta{
			name: name,
			//elem: elem,
			elem_name: elem.name, // XXX no link
			bits_size: bits_size,
		}
		mi.parts_meta << rec
	}
}

pub fn (mut mi MaterialInfo) add_elem_from_material_elem(me &MaterialElem) {
	println("MaterialInfo add_elem_from_material_elem: me: $me")

	//mi.add_elem(me.name, me.bits_size, me.elem)
	mi.add_elem("", me.bits_size, me.elem)
}

// Get part by elem name
pub fn (mi &MaterialInfo) get_elem_name(elem_name string) &MaterialElem {
	
	pt := mi.elems[elem_name]
	
	//if isnil(me) {
	//	panic(2)
	//}

	return pt
}

// From volume m3
pub fn (mut mi MaterialInfo) add_elem_from_volume(vol f32, elem &Elem) {
	println("MaterialInfo add_elem_from_volume: vol: $vol, elem.name: $elem.name")

	mi.add_elem_from_volume_name("", vol, elem)
}

// From volume m3
pub fn (mut mi MaterialInfo) add_elem_from_volume_name(name string, vol f32, elem &Elem) {
	println("MaterialInfo add_elem_from_volume_name: name: '$name', vol: $vol, elem.name: $elem.name")

	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(name, vol_bits, elem)
}

// From volume cylinder
pub fn (mut mi MaterialInfo) add_elem_from_volume_cylinder(h f32, d f32, elem &Elem) {
	mi.add_elem_from_volume_cylinder_name("", h, d, elem)
}

// From volume cylinder
pub fn (mut mi MaterialInfo) add_elem_from_volume_cylinder_name(name string, h f32, d f32, elem &Elem) {
	println("MaterialInfo add_elem_from_volume_cylinder_name: " +
		"name: '$name', h: $h, d: $d, elem.name: $elem.name")

	r2 := f32(math.pow((f64(d) / 2.0), 2.0))
	vol := h * math.pi * r2
	//vol := f32(0.0)

	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(name, vol_bits, elem)
}


// From volume mass/weight
pub fn (mut mi MaterialInfo) add_elem_from_mass(mass f32, elem &Elem) {
	println("MaterialInfo add_elem_from_mass: mass: $mass, elem.name: $elem.name")

	mi.add_elem_from_mass_name("", mass, elem)
}

// From volume mass/weight
pub fn (mut mi MaterialInfo) add_elem_from_mass_name(name string, mass f32, elem &Elem) {
	println("MaterialInfo add_elem_from_mass_name: name: '$name', mass: $mass, elem.name: $elem.name")

	vol := mass / f32(elem.density)

	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(name, vol_bits, elem)
}

pub fn (mut mi MaterialInfo) add_elem_from_size(mx f32, my f32, mz f32, elem &Elem) {
	println("MaterialInfo add_elem_from_size: mx: $mx, my: $my, mz: $mz, elem.name: $elem.name")

	mi.add_elem_from_size_name("", mx, my, mz, elem)
}

//fn (mut mi MaterialInfo) add_elem_from_size(size [3]f32, elem &Elem) {
//fn (mut pi PartsInfo) add_elem_from_size(size []f32, elem &Elem) {
//	println("PartsInfo add_elem_from_size: $size, elem.name: $elem.name")
pub fn (mut mi MaterialInfo) add_elem_from_size_name(name string, mx f32, my f32, mz f32, elem &Elem) {
	println("MaterialInfo add_elem_from_size_name: " + 
		"name: '$name', mx: $mx, my: $my, mz: $mz, elem.name: $elem.name")

	/*
	mx := size[0]
	my := size[1]
	mz := size[2]
	*/
	
	vol := mx * my * mz
	vol_bits := m3_to_bits(vol)

	println("vol: $vol, vol_bits: $vol_bits")

	mi.add_elem(name, vol_bits, elem)
}

pub fn (mi &MaterialInfo) elems_list() []&MaterialElem {
	mut ret := []&MaterialElem{}
	for k in mi.elems.keys() {
		v := mi.elems[k]
		ret << v
	}
	
	return ret
}
