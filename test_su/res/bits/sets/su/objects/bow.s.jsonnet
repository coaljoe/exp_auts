local util = import "util.libsonnet";

{
  // medieval bow
  // 70cm
  _info: {
    name: "bow",
    description: "a 70cm medieval bow",

    weight: "1 kg",
    size: "70 cm",
	length: "70 cm",
    components: "shaft, bowstring",

    materials: "wood, textile",
    materials_long: "base: wood; shaft: wood; bowstring: textile",
  },

  name: "bow",
  material: [
  // Shaft
  {
    // XXX ?
    _name: "shaft",
  	elem: "wood",
    amount: $util.from_volume(0.88),
  },
  // Bowstring
  // length: 70cm
  {
    _name: "bowstring",
  	elem: "textile",
  	amount: $util.from_volume_cylinder(2.8, util.to_size(0.7*4, "mm"),
  }
  ]
}
