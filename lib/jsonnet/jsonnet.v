module jsonnet

// XXX
$if !windows {
	#flag -ljsonnet
	#include "libjsonnet.h"
} $else {
	//#flag -ljsonnet
	//#flag -I "contrib/jsonnet"
	//#include "libjsonnet.h"
	//#include "@VMODROOT\contrib\jsonnet\libjsonnet.h"
	#flag -I @VMODROOT\contrib\jsonnet
	//#include "@VMODROOT\contrib\jsonnet\libgojsonnet.h"
	//#include "libgojsonnet.h" 
	#include "libjsonnet.h"
	#flag -L @VMODROOT\contrib\jsonnet
	//#flag -l"libjsonnet.so"
	//#flag "@VMODROOT\contrib\jsonnet\libjsonnet.cpp" 
	//#flag @VMODROOT\contrib\jsonnet\c-bindings.a 
	//#flag -l:"@VMODROOT\contrib\jsonnet\libjsonnet.so"
	#flag -l:libjsonnet.so
	//#flag -llibjsonnet
	
}

struct C.JsonnetVm {

}

fn C.jsonnet_make() &C.JsonnetVm

//fn C.jsonnet_evaluate_file(vm &C.JsonnetVm, filename charptr, error intptr) charptr
fn C.jsonnet_evaluate_file(vm &C.JsonnetVm, filename charptr, error voidptr) charptr

fn C.jsonnet_destroy(vm &C.JsonnetVm)

struct Vm {
mut:
	// Vm instance
	vm_inst &C.JsonnetVm
}

// Make VM instance
pub fn make() &Vm {
	println("jsonnet: make()")

	vm_inst := C.jsonnet_make()

	ret := &Vm{
		vm_inst: vm_inst,
	}

	println("jsonnet: done make()")

	return ret
}

pub fn (vm &Vm) evaluate_file(filename string) ?string {
	println("jsonnet: Vm evaluate_file() filename: $filename")

	c_err := int(0)
	//c_err := jsonnet.intptr{0}
	//c_err := intptr(0)
	c_res := C.jsonnet_evaluate_file(vm.vm_inst, filename.str, &c_err)
	//c_res := C.jsonnet_evaluate_file(vm.vm_inst, filename.str, c_err)
	res := ""
	unsafe {
		res = cstring_to_vstring(c_res)
	}
	println("c_err: $c_err")
	println("res: $res")
	

	// XXX
	C.free(c_res)

	println("jsonnet: done Vm evaluate_file()")

	if c_err != 0 {
		return error(res)
	} else {
		return res
	}
}

pub fn (mut vm Vm) destroy() {
	println("jsonnet: Vm destroy()")

	C.jsonnet_destroy(vm.vm_inst)

	vm.vm_inst = &C.JsonnetVm(0) // XXX

	println("jsonnet: done Vm destroy()")
}

// Simply process file and return the result
pub fn load_file(filename string) ?string {
	println("jsonnet: load_file() filename: $filename")
	
	mut vm := make()
	defer {
		vm.destroy() // XXX
	}
	
	res := vm.evaluate_file(filename) or {
		//println("XXX error")
		//return error(res1)
		return err // XXX Error propagation
		//return none
	}

	println("jsonnet: done load_file()")
	
	return res
}

/*
pub fn make() &C.JsonnetVm {
	println("jsonnet: make()")

	vm := C.jsonnet_make()

	println("jsonnet: done make()")

	return vm
}


pub fn evaluate_file(vm &C.JsonnetVm, filename string) string {
	println("jsonnet: evaluate_file() filename: $filename")

	mut res := ""
	c_err := int(0)
	c_res := C.jsonnet_evaluate_file(vm, filename.str, &c_err)
	res = cstring_to_vstring(c_res)

	println("c_err: $c_err")
	println("res: $res")

	println("jsonnet: done evaluate_file()")

	return res
}
*/
