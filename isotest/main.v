module main

import spytheman.vperlin as perlin
import time
import rand
import flag
import os

import bits
import mathex
		

fn main() {
	println("main()")

	if false {
		mut vo1 := new_voxel_obj(10, 10, 10)
		println("d1: ${vo1.data[0][0][0]}")
		vo1.data[0][0][0] = 1
		vo1.data[4][0][0] = 1
		println("d2: ${vo1.data[0][0][0]}")

		mut vo2 := new_voxel_obj(5, 10, 20)
		println("d3: ${vo2.data[0][0][0]}")
		vo2.data[4][9][19] = 1
		println("d4: ${vo2.data[0][0][0]}")

		mut vs1 := new_voxel_space(20, 20, 20)
		//n1 := new_voxel_obj_node_from_obj(0, 0, 0, vo1)
		n1 := new_voxel_obj_node_from_obj(1, 0, 0, vo1)
		vs1.nodes << n1

		//println("vs1.nodes: ${vs1.nodes}")

		v := vs1.is_empty(0, 0, 0)
		//vs1.is_empty(1, 0, 0)
		//vs1.is_empty(2, 0, 0)

		println("v: $v")

		box1 := mathex.new_aabb()

		println("box1: $box1")
	
		//pp(2)
	}

	//diag_line_points(0, 0, 1, 1)
	//diag_line_points(0, 0, 2, 2)
	//diag_line_points(0, 0, 1, 2)
	//diag_line_points(0, 0, 2, 1)

	//diag_line_points(0, 0, 0, 1)
	//diag_line_points(0, 0, 0, 2)

	//diag_line_points(0, 0, 0, -1)
	//diag_line_points(0, 0, 1, 0)
	//diag_line_points(0, 0, -1, 0)

	//diag_line_points(0, 0, -1, -1)
	//diag_line_points(0, 0, -2, -2)

	//diag_line_points(0, 0, 1, 2)
	//diag_line_points(0, 0, -1, -2)

	// 2 axis inv
	//diag_line_points(0, 0, -1, -2)

	// 1 axis inv
	//diag_line_points(0, 0, -1, 2)
	//diag_line_points(0, 0, 1, -2)

	//diag_line_points(10, 10, 20, 12)
	//diag_line_points(0, 0, 10, 2)

	//pp(2)

	// XXX fixme
	mut f_fullscreen := false
	_ = f_fullscreen

	if true {
		// Read args
		default_res_path := "res/bits"
		default_set_name := "me"

		mut fp := flag.new_flag_parser(os.args)
		fp.application("isotest")

		fp.skip_executable()

		f_res_path := fp.string("res_path", 0, default_res_path, "set custom res_path")
		f_set_name := fp.string("set_name", 0, default_set_name, "set custom set_name")
		_ = f_set_name
		f_fullscreen = fp.bool("fullscreen", `f`, false, "fullscreen mode")
	   	f_help := fp.bool("help", `h`, false, "show help")

    	additional_args := fp.finalize() or {
			println(err)
			println(fp.usage())
			return
		}

		if f_help {
			println(fp.usage())
			exit(0)
		}
		
		println("f_res_path: $f_res_path")
		println(additional_args.join_lines())

		mut opts := bits.new_bits_opts()
		//opts.res_paths << "res/mybits"
		opts.set_name = "me"

		opts.args = os.args

		bits.init_bits(opts)

		//bits.init_bits(res_path: f_res_path, set_name: f_set_name)

		//pp(2)
		
	} else {
		bits.bits_main_init()
	}

	//pp(2)
	
	bits.bits_main()
	//pp(2)

	// XXX init context
	//mut ctx := new_context()
	ctx = new_context()
	init_default_context() // XXX fixme?
	//init_context()

	// XXX init app
	mut app := new_app()
	
	app.init(800, 600, f_fullscreen, "auts")
	//app.init(800, 600, false, "auts")
	
	// XXX game
	init_game()

	//mut f := new_field()
	mut f := game.field

	f.tiles_path = "res/images/tiles/test2"

	//f.generate(4, 4)
	//f.generate(24, 24)
	//f.generate(24, 48)
	//f.generate(32, 96)
	//f.generate(32, 32)
	f.generate(128, 128)

	//f.cells[1][1].z = 100
	f.cells[1][1].z = 10
	println("z 0 0: ${f.cells[0][0].z}")
	println("z 1 1: ${f.cells[1][1].z}")

	// XXX
	f.cells[2][1].z = 9
	f.cells[3][1].z = 1

	f.cells[8][8].z = 10

	f.cells[0][1].tile_id = 1
	//f.cells[0][2].tile_id = 1
	//f.cells[0][3].tile_id = 2

	//f.cells[1][1].tile_id = 1
	//f.cells[2][2].tile_id = 2


	// Randomize tiles
	for y in 0..f.h {
		for x in 0..f.w {
			//tile_num := 3
			//tile_num := 5
			tile_num := 6
			//tile_num := f.view.tiles.keys().len - 1
			
			mut n := rand.intn(tile_num)?
			
			//scale := 1.0
			//scale := 0.1
			//scale := 0.3
			scale := 0.05
			mag := f32(tile_num)
			nv := perlin.noise2d(f32(x) * scale, f32(y) * scale)
			nv_norm := (nv + 1.0) / 2.0
			xnv := nv_norm * mag
			//println(xnv)
			if xnv > tile_num {
				pp(2)
			}
			//mut n := int(xnv)
			mut noise_n := int(xnv)

			

			// Randomize with random
			//if rand.intn(100) > 80 {
			if rand.intn(100)? > 70 {
			//if rand.intn(100) > 30 {
				//n = rand.intn(tile_num)
				n = noise_n
			}
			
			f.cells[x][y].tile_id = n


			if false {

				scale2 := 0.02
				nv2 := perlin.noise2d(f32(x) * scale2, f32(y) * scale2)

				// XXX
				//f.cells[x][y].z = noise_n * 4
				//f.cells[x][y].z = int(nv_norm * 10)
				xmag := 2
				//xmag := 4
				//xmag := 8
				//f.cells[x][y].z = int((nv * xmag) * 10)
				f.cells[x][y].z = int((nv2 * xmag) * 10)

			}

			// XXX place stone
			if rand.intn(100)? < 1 {
				//max_variant := 4
				max_variant := 1

				mut s := new_map_obj()
				//s.load()
				//s.path = "stones/stone1"
				s.path = "trees/tree1"
				s.variant = rand.intn(max_variant)?
				//s.place_at(0, 0)
				s.place_at(x, y)
				s.spawn()

				// XXX
				s.view.update(0)
			}
		}
	}

	max_variant := 4

	mut s := new_map_obj()
	//s.load()
	s.path = "stones/stone1"
	s.variant = rand.intn(max_variant)?
	s.place_at(0, 0)
	s.spawn()

	// XXX
	s.view.update(0)


	//f.spawn()
	game.start()

	//d := new_drawer(mut ctx)
	mut d := ctx.drawer

	tex := d.load_image_sdl_tex("res/images/heightmap.png")
	_ = tex


	// Sprite test

	mut spr1 := new_sprite()
	spr1.load("res/objects/walls/wall1/image_0.png")

	//spr1.x = 32

	/*
	{
		// Position sprite
		px := 8
		py := 8
		// Center at tile
		spr1.x = (px * c_cell_w) + c_cell_w / 2
		spr1.y = (py * (c_cell_h / 2)) + c_cell_h / 2
	}
	*/

	mut spr2 := new_sprite()
	spr2.load("res/objects/walls/wall1/image_0.png")

	{
		// Position sprite
		px := 8
		py := 9 // XXX
		//py := 8 // XXX
		//px := 2
		//py := 2
		// Center at tile
		//spr2.x = (px * c_cell_w) + c_cell_w / 2
		//spr2.y = (py * (c_cell_h / 2)) + c_cell_h / 2
		//spr2.y = (py * (c_cell_h / 2)) + c_cell_h / 2
		//x1, y1 := cell_to_world_pos(px, py)
		x1, y1 := cell_to_world_pos_center(px, py)

		println("x1: $x1, y1: $y1")
		//pp(2)
		
		spr2.x = x1
		spr2.y = y1
	}
	
	mut wall1 := new_wall()
	wall1.spawn()

	wall1.set_cx(8)
	wall1.set_cy(8)

	//wall1.obj.set_cx(2)
	//wall1.obj.set_cy(1)

	//wall1.obj.set_cx(2)
	//wall1.obj.set_cy(1)

	// XXX fixme
	wall1.view.update(0)

	// WallTool

	//game.walltool.build_wall(10, 10, 12, 12)
	//game.walltool.build_wall(20, 20, 20, 30)
	//game.walltool.build_wall(10, 10, 20, 12)


	
	//game.walltool.build_wall(10, 10, -1, -1)
	//game.walltool.build_wall(11, 11, -1, -1)
	//game.walltool.build_wall(12, 12, -1, -1)
	//game.walltool.build_wall(13, 13, -1, -1)

	/*
	game.walltool.build_wall(10, 10, -1, -1)
	game.walltool.build_wall(11, 10, -1, -1)
	game.walltool.build_wall(12, 10, -1, -1)
	game.walltool.build_wall(13, 10, -1, -1)
	*/

	game.walltool.build_wall(10, 11, -1, -1)
	game.walltool.build_wall(10, 12, -1, -1)
	game.walltool.build_wall(10, 13, -1, -1)
	game.walltool.build_wall(10, 14, -1, -1)
	//game.walltool.build_wall(10, 10, 13, 10)


	//game.walltool.build_wall(8, 12, 20, 12)
	
	//game.walltool.build_wall(5, 12, 5, 13)
	game.walltool.build_wall(5, 12, 6, 14)

	//// Misc

	//ctx.vars.scroll_speed = 0
	ctx.vars.mouse_scroll = false

	game.hud.build_mode = .wall

	for app.step() {
		//app.step()
		//h.update(1)
		//game.update(1)
		//game.update(1.0 / 60.0)
		game.update(1.0 / 30.0)

		d.clear()

		//f.view.draw()
		

		//d.draw_text("test", 0, 0, d.color_white)
		
		game.draw()

		//d.draw_sdl_tex(tex, 0, 0)

		px, py := cell_to_screen_pos(1, 1)
		//px, py := cell_to_screen_pos2(1, 1)
		_ = px
		_ = py
		
		//d.draw_sdl_tex(tex, px, py)

		//spr1.draw()
		//spr2.draw()

		//wall1.view.draw()

		app.flip()

		//time.sleep_ms(20)
		//time.sleep_ms(33) // 30 fps?
		time.sleep(33 * time.millisecond) // 30 fps?
		//time.sleep_ms(200)
		//pp(2)
	}

	app.quit()

	println("done")
}
