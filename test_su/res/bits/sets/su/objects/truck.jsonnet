{
  // test truck
  // Tatra 148
  
  // weight: ~10,000 kg
  // num tires: 10 (3 axel)
  // tires: 20"
  // engine weight: 845 kg


  name: "truck",
  
  info: {
    // XXX total model mass?
    model_mass: 10000,
  },
  
  material: [
  
  // Wheels -> Tires
  {
  	name: "wheels",
  	type: "from_mass",
  	params: 20 * 10, // 10 * 20 kg
  	elem: "rubber",
  },
  
  // Engine block:
  
  // Engine
  {
    name: "engine",
  	type: "from_mass",
  	params: 845, // 845 kg
  	elem: "steel",
  },
 
  // Transmission
  {
    name: "transmission",
  	type: "from_mass",
  	params: 300, // 300 kg
  	elem: "steel",
  },
  
  
  // The rest:
  {
    //name: "rest",
  	type: "from_rest_mass", // XXX
  	//params: [$self.info.model_mass - $self.mass],
  	//params: eval($self.info.model_mass - $self.mass),
  	//params: 8600, // XXX pre-calculated FIXME
  	elem: "steel",
  },
  
  ],
}
