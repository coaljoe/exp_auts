module bits

__global (
	bits_ctx &Context
)

// XXX quick access to global context
[inline]
pub fn ctx() &Context {
	return bits_ctx
}
