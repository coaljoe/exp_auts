module bits

import debug { pp }
import flag

pub struct BitsOpts {
pub mut:
	res_paths []string
	set_name string
	// Load common / base(?)
	//load_common bool = true // XXX?
	load_base bool = true

	// XXX sys.argv to parse on init
	args []string
}

// XXX new default bitsopts
pub fn new_bits_opts() BitsOpts {
	bo := BitsOpts{
		res_paths: [
			"res/bits_base"
			"res/bits",
		],
		load_base: true,
	}

	return bo
}

/*
struct Bits {
	res_path string
}

fn new_bits() &Bits {

}
*/

pub fn parse_args(args []string) {
}

pub fn init_bits(opts BitsOpts) {
	println("bits: init_bits() opts: $opts")

	// Override
	mut o_res_paths := opts.res_paths
	mut o_set_name := opts.set_name

	// Parse args
	if opts.args.len > 0 {
		// Read args

		mut fp := flag.new_flag_parser(opts.args)
		//fp.skip_executable()

		// XXX FIXME
		f_res_paths := fp.string("res_paths", 0, '', "set custom res_paths")
		f_set_name := fp.string("set_name", 0, '', "set custom set_name")

		/*
		// XXX?
    	additional_args := fp.finalize() or {
			println(err)
			println(fp.usage())
			return
		}
		*/

		// XXX ?
		//additional_args := fp.remaining_parameters()
		
		println("f_res_paths: $f_res_paths")
		//println(additional_args.join_lines())

		//mut res_paths := []string{}

		// XXX override res_paths from flags/args
		if f_res_paths.len > 0 {
			o_res_paths = f_res_paths.split(";")
			println("f_res_paths: '$f_res_paths'")
			pp(4)
		}

		if f_set_name.len > 0 {
			o_set_name = f_set_name
		}
	}

	// Init
	bits_ctx = bits.new_context()
	// XXX Set res paths
	//bits_ctx.res_path.add_paths(opts.res_paths)
	bits_ctx.res_path.add_paths(o_res_paths)
	//bits_ctx.sets_path = opts.res_path + "/sets"
	//bits_ctx.set_name = opts.set_name
	bits_ctx.set_name = o_set_name
	//bits_ctx.set_path = bits_ctx.res_path + "/sets/" + opts.set_name
	//bits_ctx.set_path = "bits/sets/" + bits_ctx.set_name // XXX relative to res?
	bits_ctx.set_path = "sets/" + bits_ctx.set_name // XXX relative to res?
	

	// XXX TODO: path verify?

	// XXX Verify
	
	if bits_ctx.set_name == "" {
		pps("set_name is empty: $bits_ctx.set_name")
	}

	// Load elem lib from set
	mut l := new_elem_lib_loader()
	_ = l

	/*
	// XXX
	if opts.load_common {
		println("bits: loading common...")
		l.load(bits_ctx.res_path + "/sets/common/elems.json")
	}
	*/
	
	//l.load("res/bits/sets/me/elems.json")
	//l.load(bits_ctx.set_path + "/elems.json")

	mut sl := new_set_loader()
	sl.load(bits_ctx.set_path)

	println("done init_bits()")
}
