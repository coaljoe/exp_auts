{
	// soviet union [game] resources
	
	resources: [
	{
		// Processed / construction wood
		// Accumulated via wood processing
		// ? created in wood factory / sawmill building / processor ?
		name: "wood",
		elem_type: "wood", // ?
	},
	{
		// [raw] Wood logs / Unprocessed wood
		// Accumulated via wood harvesting
		// ? created in forest / trees
		// Unsuitable for construction
		//name: "raw wood",
		name: "logs",
		elem_type: "wood", // ?
	},

	]
}
