{
  // offroad tire

  // weight: 25 kg
  // size: 20"

  name: "offroad_tire",

  info: {
    // XXX
    cat_id: "3cba617b-c567-48bc-9875-0e1aa9a59efc",
    cat_name: "Test offroad tire",
    cat_description: "test offroad tire",
    cat_notes: 'tire size: 20"',
    model_mass: 25,  // XXX?
  },

  material: [

    {
      name: "tire",
      type: "from_mass",
      params: 25,  // 25 kg
      elem: "rubber",
    },

  ],
}
