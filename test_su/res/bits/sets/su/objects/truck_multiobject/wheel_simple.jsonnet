{
  // test wheel simple
  // Tatra 148 wheel
  
  // weight: 20 kg
  // num tires: 10 (3 axel)
  // tires: 20"
  
  //name: "tires",
  name: "wheel",
  type: "from_mass",
  params: 20, // 20 kg
  elem: "rubber",
}
