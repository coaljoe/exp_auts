{
  // test wheel
  // Tatra 148 wheel

  // weight: 20 kg
  // num tires: 10 (3 axel)
  // tires: 20"


  name: "wheel",

  info: {
    // XXX
    cat_id: "14581308-1add-4020-d89b-c59818a236f3",
    cat_name: "Tatra 148 wheel",
    cat_description: "test wheel",
    cat_notes: 'Tatra 148 or generic wheel(?); tire size: 20"',
    model_mass: 20,  // XXX?
  },

  material: [

    // Wheels -> Tires
    {
      name: "tires",
      type: "from_mass",
      params: 20,  // 20 kg
      elem: "rubber",
    },

  ],
}
