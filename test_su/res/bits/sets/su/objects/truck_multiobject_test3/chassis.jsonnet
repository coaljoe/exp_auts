{
  // test truck
  // Tatra 148

  // weight: ~10,000 kg (10,800)
  // num tires: 10 (3 axel)
  // tires: 20"
  // engine weight: 845 kg
  // dump truck body weight: 2000 kg


  name: "chassis",

  info: {
    // XXX
    cat_id: "262b06ed-ce61-4733-a12e-5996480000b1",
    cat_name: "Tatra 148 Chassis",
    cat_description: "test truck chassis",
    cat_notes: "",
    // XXX total model mass?
    model_mass: 8000,
  },

  material: [

    // Wheels -> Tires

    // Engine block:

    // Engine
    {
      name: "engine",
      type: "from_mass",
      params: 845,  // 845 kg
      elem: "steel",
    },

    // Transmission
    {
      name: "transmission",
      type: "from_mass",
      params: 300,  // 300 kg
      elem: "steel",
    },

    // The rest:
    {
      //name: "rest",
      type: "from_rest_mass",  // XXX
      elem: "steel",
    },
  ],

  ],
}
