module bits

const (
	//c_bit_size = 10
	c_bit_size = 50
	c_bits_in_m = 100 / c_bit_size
	c_bits_in_m3 = c_bits_in_m * c_bits_in_m * c_bits_in_m // pow 3
)
