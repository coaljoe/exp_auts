{
	elems: [
	{
		// Brown iron ore
		// https://en.wikipedia.org/wiki/Limonite
		// fine/processed ?
		name: "iron_ore",
		elem_type: "iron_ore",
		density: 5100, // https://www.engineeringtoolbox.com/density-solids-d_1265.html
		composition: [
			// XXX raw fine ore
			// https://en.wikipedia.org/wiki/Beneficiation
			// https://en.wikipedia.org/wiki/Mineral_processing
			// https://en.wikipedia.org/wiki/Ore_concentrate
			// https://en.wikipedia.org/wiki/Refining_(metallurgy)
			//[1.0, "limonite"],
			[0.55, "iron"], // https://en.wikipedia.org/wiki/Iron_ore
		],
	},
	{
		name: "iron",
		elem_type: "iron",
		density: 7870, // wiki
		composition: [
			[1.0, "iron"]
		],
	},
	{
		// Raw wood (unprocessed, logs etc)
		name: "raw_wood",
		elem_type: "raw_wood", // ?
		density: 700, // wiki
	},
	{
		// Construction wood
		name: "wood",
		elem_type: "wood",
		density: 700, // wiki
	},
	{
		// XXX test
		// Crushed wood [pile]
		name: "wood_debris",
		elem_type: "wood_debris",
		density: 700, // wiki
		aggregate_state: "crushed",
	},
	]
}
