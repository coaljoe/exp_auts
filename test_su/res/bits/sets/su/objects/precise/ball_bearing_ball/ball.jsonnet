{
  // test ball for ball bearing
  // Tatra 148 wheel
  
  // weight: 20 kg
  // num tires: 10 (3 axel)
  // tires: 20"


  name: "ball_bearing_ball",
  
  info: {
    cat_id: "14581308-1add-4020-d89b-c59818a236f3",
    cat_name: "Ball bearing",
    cat_description: "test ball bearing",
    cat_notes: "Tatra 148 or generic wheel(?); tire size: 20\"",
    model_mass: 20, // XXX?
  },
  
  material: [
  
  // Wheels -> Tires
  {
  	name: "tires",
  	type: "from_mass",
  	params: 20, // 20 kg
  	elem: "rubber",
  },
   
  ],
}
