module main

interface GenericProcessorI {
	process(ob &Object)
}

struct GenericProcessor {
	name string
}

struct GenericCrusherProcessor {
	//GenericProcessor
mut:
	name string
}

fn new_generic_crusher_processor() &GenericCrusherProcessor {
	mut gcp := &GenericCrusherProcessor{}
	gcp.name = "generic_crusher_processor"
	return gcp
}

fn (gpc &GenericCrusherProcessor) process(ob &Object) {
	println("processing object: ${ob.name}")

	if ob.elem.name == "stone" {
		println("> create smaller stones")
		println("> create sand")
		println("> (visual) create dust")
		println("> (audio) emit sound")
	} else {
		println("nothing happens")
	}
}
