{
  // test ball bearing assembly

  name: "ball_bearing (assembly)",

  _info: {
    name: "ball bearing assembly",
    description: "a 10cm ball bearing",
    notes: "8 balls?",

    //weight: "1 kg",
    //size: "70 cm",
	//length: "70 cm",
    components: "balls, rings",

    materials: "metal",
    materials_long: "",
  },
  
  cat: {
    id: "14581308-1add-4020-d89b-c59818a236f3",
    name: "Ball bearing",
    description: "test ball bearing",
    notes: "",
  },

  // Bill of materials ?
  material_list: [
    // Balls
    {"ball_bearing_ball", 8}, // 45 deg.

    // XXX internal ?
    // this is not produced separately / as output ?
    // for internal use / processes only ?
    
    // Rings
    {"internal_outer_ring"},
    {"internal_inner_ring"},
    // Holder ring
    {"internal_holder"},
  ],
}
