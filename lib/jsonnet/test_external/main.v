module main

import lib.jsonnet

fn main() {
	println("main()")

	res := jsonnet.load_file("../test/test1.jsonnet") or {
		panic(err)
	}

	println("res: $res")

	println("done")
}
