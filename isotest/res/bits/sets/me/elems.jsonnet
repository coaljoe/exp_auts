{
	"elems": [
	{
		"name": "stone",
		"elem_type": "stone",
		"density": 2700 // wiki:limestone
	},
	{
		"name": "crushed_stone",
		"elem_type": "stone",
		"density": 2700, // wiki:limestone
		"aggregate_state": "crushed"
	},
	{
		"name": "sand",
		"elem_type": "sand",
		"density": 1600 // wiki
	},
	{
		"name": "glass",
		"elem_type": "glass",
		"density": 2500 // wiki
	},
	{
		"name": "wood",
		"elem_type": "wood",
		"density": 700, // wiki
		"flameable": true
	},
	{
		"name": "textile",
		"elem_type": "textile",
		"density": 1500, // hemp/flax m3 density
		"flameable": true
	},	
	{
		"name": "iron", // XXX wrought iron
		"elem_type": "iron",
		"density": 7870, // wiki (iron)
		"notes": "wrought iron"
	},
	{
		"name": "water",
		"elem_type": "water",
		"density": 1000, // wiki
		"liquid": true
	},
	{
		"name": "concrete",
		"elem_type": "concrete",
		"density": 2400 // wiki
	}
	]
}
