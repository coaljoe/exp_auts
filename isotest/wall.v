module main

import bits

[heap]
struct Wall {
	Obj // Embed
mut:
	//obj &Obj // Embed
	bits_obj &bits.Object

	// Map obj
	//cx int
	//cy int

	// Size
	sx int
	sy int
	sz int

	image_variant int
	// XXX ?
	wall_name string
	//wall_type string

	view &WallView
}

fn new_wall() &Wall {
	println("new_wall()")

	mut w := &Wall{
		//obj: new_obj(),
		Obj: new_obj(),
		bits_obj: &bits.Object(0),

		sx: 1,
		sy: 1,
		sz: 6, // units/tiles/steps?

		//image_variant: 1,
		wall_name: "test3",

		view: &WallView(0),
	}

	// Bits Object
	{
		mut bo := bits.new_object()
		bo.name = "wall"
	
		bo.mat_info.add_elem_from_size(w.sx, w.sy, w.sz, bits_ctx.elem_lib["stone"])
		w.bits_obj = bo
	}

	return w
}

fn (w &Wall) get_near_wall_name(cx int, cy int) ?string {
	println("Wall get_near_wall_name cx: $cx, cy: $cy")

	// XXX
	if !game.field.check_coords(cx, cy) {
		panic("bad coords")
	}

	// XXX left ?
	//c := game.field.get_cell(cx-1, cy-1)

	c, ok := game.field.get_cell_check(cx-1, cy-1)

	if ok {
		if isnil(c.wall) {
			return ""
		}

		return c.wall.wall_name
	}

	return ""
}

//fn (mut w Wall) place_at_check(cx, cy int) ?string {
fn (mut w Wall) place_at_check(cx int, cy int) ? {
//fn (mut w Wall) place_at_check(cx, cy int) err {
	mut c := game.field.get_cell(cx, cy)
	if !c.open {
		println("return error")
		return error("can't place wall: cell is not open: cx: $cx, cy: $cy")
	}

	w.set_cx(cx)
	w.set_cy(cy)

	// XXX
	if c.z != 0 {
		w.set_zlevel(c.z)
	}

	//mut c := &game.field.cells[cx][cy]
	//mut c := game.field.get_cell(cx, cy)
	c.wall = w
	c.open = false


	// Check near cell
	s := w.get_near_wall_name(cx, cy)?
	println("s: $s")

	if s != "" {
		w.image_variant = 1

		//pp(2)
	}

	println("return none")
	//return none
	return
}

fn (mut w Wall) place_at(cx int, cy int) {
	println("Wall place_at cx: $cx, cy: $cy")

	w.place_at_check(cx, cy) or {
		println("err:")
		println(err)
		println(isnil(err))
		panic(err)
		return
	}
}

fn (mut w Wall) spawn() {
	println("Wall spawn()")

	w.view = new_wallview(w)
	w.view.spawn()
}

fn (mut w Wall) update(dt f32) {

}
