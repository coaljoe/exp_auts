{
  // test ball bearing ball inner ring

  name: "ball_bearing_inner_ring",

  _info: {
    name: "ball bearing inner ring",
    description: "a ball berings inner ring",
    notes: "",

    //weight: "1 kg",
    //size: "70 cm",
	//length: "70 cm",
    //components: "balls, rings",

    materials: "metal",
    materials_long: "",
  },
  
  cat: {
    id: "14581308-1add-4020-d89b-c59818a236f3",
    name: "Ball bearing inner ring",
    description: "test ball bearing inner ring",
    notes: "",
  },

  materials: [
    {"metal", 0.9},
  ],
}
