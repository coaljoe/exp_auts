module main

[heap]
struct Field {
mut:
	w int
	h int
	tiles_path string
	cells [][]Cell
	generated bool
	
	view &FieldView
}

fn new_field() &Field {
	println("new_field()")
	f := &Field{
		tiles_path: "res/images/tiles/test1",
		view: &FieldView(0),
	}
	return f
}

fn (mut f Field) generate(w int, h int) {
	println("Field generate w: $w, h: $h")
	
	f.w = w
	f.h = h

	println("generate cells...")
	f.cells = [][]Cell{ len: w } // Rows
	//for y in 0..h {
	//	f.cells[y] = []Cell{ len: h } // Cols
	//}

	for i in 0..f.cells.len {
		f.cells[i] = []Cell{ len: h } // Cols
	}

	// Create cells
	for y in 0..f.h {
		for x in 0..f.w {
			mut c := new_cell()
			c.x = x
			c.y = y
			f.cells[x][y] = c
		}
	}

	f.generated = true
}

fn (f &Field) check_coords(cx int, cy int) bool {
	if cx < 0 || cx > f.w-1 || cy < 0 || cy > f.h-1 {
		return false
	}
	return true
}

fn (f &Field) has_cell(cx int, cy int) bool {
	return f.check_coords(cx, cy)
}

// Get cell as reference
fn (f &Field) get_cell(cx int, cy int) &Cell {
	//println("Field get_cell cx: $cx, cy: $cy")

	///*
	//if !f.check_coords(cx, cy) {
	if !f.has_cell(cx, cy) {
		println("panic: bad coords: cx: $cx, cy: $cy")
		panic("error")
	}

	c := &f.cells[cx][cy]
	return c
	//*/

	/*
	f.get_cell_check(cx, cy) or {
		// XXX return nil?
		return &Cell(0)
	}
	*/
}

// Go style checks ?
fn (f &Field) get_cell_check(cx int, cy int) (&Cell, bool) {
	//println("Field get_cell_check cx: $cx, cy: $cy")

	if !f.has_cell(cx, cy) {
		//println("no such cell, return nil")
		return &Cell(0), false
	}

	return f.get_cell(cx, cy), true
}

/*
fn (f &Field) get_cell_check(cx int, cy int) ?&Cell {
	if !f.check_coords(cx, cy) {
		println("panic: bad coords: cx: $cx, cy: $cy")
		return error("bad coords: cx: $cx, cy: $cy")
	}

	c := &f.cells[cx][cy]
	return c
}
*/

fn (f &Field) width_px() int {
	return f.w * c_cell_w
}

fn (f &Field) height_px() int {
	return f.h * c_cell_h
}

fn (mut f Field) spawn() {
	f.view = new_fieldview(f)
	f.view.spawn()
}

fn (mut f Field) draw() {
	f.view.draw()
}

fn (mut f Field) update(dt f32) {

}
