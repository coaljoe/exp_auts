module main

enum HudBuildMode {
	tile
	wall
}

struct Hud {
mut:
	mx    int
	my    int

	mcx   int
	mcy   int

	build_mode HudBuildMode

	// XXX ?
	tex_tile_cursor &Texture
}

fn new_hud() &Hud {
	println("new_hud()")

	h := &Hud{
		build_mode: .tile,
		tex_tile_cursor: &Texture(0),
	}

	return h
}

//fn (mut h Hud) init() {
fn (mut h Hud) load() {
	mut tex := new_texture()
	path := "res/images/hud/tile_cursor.png"
	tex.load(path)

	h.tex_tile_cursor = tex
}

fn (mut h Hud) init() {
	h.load()
}

// Check if cx, cy are within field
fn (mut h Hud) check_coords(cx int, cy int) bool {
	f := game.field
	if cx < 0 || cx > f.w-1 || cy < 0 || cy > f.h-1 {
		return false
	}
	return true
}

fn (mut h Hud) place_tile(tile_id int, cx int, cy int) {
	if !h.check_coords(cx, cy) {
		return
	}
	
	game.field.cells[cx][cy].tile_id = tile_id
}

fn (mut h Hud) can_place_wall(cx int, cy int) bool {
	if !h.check_coords(cx, cy) {
		return false
	} 

	mut c := game.field.get_cell(cx, cy)

	if !c.open {
		// Cell is not open/empty
		return false
	}

	return true
}

fn (mut h Hud) place_wall(cx int, cy int) {
	if !h.check_coords(cx, cy) {
		return
	}
	
	mut wall := new_wall()

	wall.place_at(cx, cy)
	
	wall.spawn()
	
	//wall.obj.set_cx(cx)
	//wall.obj.set_cy(cy)

	// XXX upd sprite
	wall.view.update(0)
}

fn (mut h Hud) change_elevation(cx int, cy int, inc_v int) {
	if !h.check_coords(cx, cy) {
		return
	}
	
	// XXX
	mut c := &game.field.cells[cx][cy]
	if c.open {
		c.z += inc_v
		// clamp
		
		if c.z > c_cell_max_zsteps {
			c.z = c_cell_max_zsteps
		}
		if c.z < 0 {
			c.z = 0
		}
	}
}

fn (mut h Hud) draw() {

	s := "mx: $h.mx, my: $h.my"

	ctx.drawer.draw_text(s, 20, 80, ctx.drawer.color_white)

	s2 := "mcx: $h.mcx, mcy: $h.mcy"
	ctx.drawer.draw_text(s2, 20, 100, ctx.drawer.color_white)

	wx, wy := screen_to_world_pos(h.mx, h.my)
	s3 := "wx: $wx, wy: $wy"
	ctx.drawer.draw_text(s3, 20, 120, ctx.drawer.color_white)

	// Draw cursor

	//if h.mcx >= 0 && h.mcy >= 0 {
	if game.field.check_coords(h.mcx, h.mcy) {

		/*
		x := h.mcx * c_tile_w
		y := h.mcy * (c_tile_h / 2)

		game.field.view.draw_tile(2, x, y)
		*/

		c := &game.field.cells[h.mcx][h.mcy]

		// XXX fixme ?
		xh := c.z * c_cell_zstep

		//game.field.view.draw_tile_cell_pos_tex(h.tex_tile_cursor, h.mcx, h.mcy, 0)
		game.field.view.draw_tile_cell_pos_tex(h.tex_tile_cursor, h.mcx, h.mcy, xh)
		//game.field.view.draw_tile_cell_pos_tex(h.tex_tile_cursor, h.mcx, h.mcy, 100)

		//game.field.view.draw_tile_cell_pos(2, h.mcx, h.mcy, 0)
		//game.field.view.draw_tile_cell_pos(2, 0, 0, 0)
		//game.field.view.draw_tile_cell_pos(2, 1, 1, 0)
		//game.field.view.draw_tile_cell_pos(2, 2, 1)
	}
}

fn (mut h Hud) update(dt f32) {
	mx, my := ctx.app.get_mouse_state()

	h.mx = mx
	h.my = my

	//mcx, mcy := mouse_to_cell_pos(mx, my)
	//mcx, mcy := screen_to_cell_pos(mx, my)
	//mcx, mcy := screen_to_cell_pos2(mx, my)
	mcx, mcy := screen_to_cell_pos3(mx, my)

	h.mcx = mcx
	h.mcy = mcy


	// Draw tiles
	key := C.SDLK_SPACE
	if ctx.app.is_key_pressed(key) {

		if h.build_mode == .tile {
			// Place tile
			h.place_tile(2, h.mcx, h.mcy)
		} else if h.build_mode == .wall {
			if h.can_place_wall(h.mcx, h.mcy) {
				h.place_wall(h.mcx, h.mcy)
			}
		}
	}

	if ctx.app.is_key_pressed(C.SDLK_a) {
		h.change_elevation(h.mcx, h.mcy, 1)
	}

	if ctx.app.is_key_pressed(C.SDLK_z) {
		h.change_elevation(h.mcx, h.mcy, -1)
	}
}
