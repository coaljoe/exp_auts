module fixed

fn test_fixed() {
	f := new_i(1, 1)

	println("f: $f")
	println("f.float: ${f.float()}")

	{
		println("add:")
		
		a := new_i(11, 1)
		b := new_i(22, 1)

		r := a.add(b)

		println("a.float: ${a.float()}")
		println("b.float: ${b.float()}")

		println("r: $r")
		println("r: ${r.float()}")
	}

	{
		println("floating point:")

		f1 := new_f(1.1)
		f2 := new_f(2.2)
		f3 := new_f(0.01)
		f4 := new_f(-0.01)
		f5 := new_f(0.0)
		//f6 := new_f(f64('NaN'))

		println("f1.float: ${f1.float()}")
		println("f2.float: ${f2.float()}")
		println("f3.float: ${f3.float()}")
		println("f4.float: ${f4.float()}")
		println("f5.float: ${f5.float()}")
	}

	{
		println("mul:")

		f1 := new_f(1.1)
		f2 := new_f(10)

		r := f1.mul(f2)

		println("r.float: ${r.float()}")
	}

	{
		println("dev:")

		f1 := new_f(1.1)
		f2 := new_f(2.0)

		r := f1.div(f2)

		println("r.float: ${r.float()}")
	}
	
	println("done")
}