module jsonnet

fn test_jsonnet() {
	println("test: test_jsonnet()")

	/*
	vm := make()

	res := evaluate_file(vm, "test/test1.jsonnet")
	*/

	vm := make()

	_ = vm.evaluate_file("test/test1.jsonnet") or {
		panic(err)
	}

	_ = vm.evaluate_file("test/test1.json") or {
		println("there's an error: $err")
		//panic(err)
		return
	}

	println("test: done test_jsonnet()")
}

fn test_load_file() {
	println("test: load_file()")
	
	_ = load_file("test/test1.json") or {
		println("? err: $err")
		//panic(err)
		return
	}

	_ = load_file("test/test1.jsonnet") or {
		panic(err)
	}

	println("test: done load_file()")
}
