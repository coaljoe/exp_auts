module bits

interface GenericProcessorI {
	process(ob &Object)
}

struct GenericProcessor {
	name string
}

struct GenericCrusherProcessor {
	//GenericProcessor
mut:
	name string
}

fn new_generic_crusher_processor() &GenericCrusherProcessor {
	mut gcp := &GenericCrusherProcessor{}
	gcp.name = "generic_crusher_processor"
	return gcp
}

fn (gpc &GenericCrusherProcessor) process(mut ob Object) {
	println("processing object: ${ob.name}")

	//if ob.elem.name == "stone" {
	//if ob.has_elem("stone") {
	if ob.mat_info.has_elem_name("stone") {
		println("> create smaller stones")

		//crush_stone_object(ob)
		//crush_stone_object2(ob)
		//crush_stone_object2_part(ob, ob.stone_part)
		//crush_stone_object_part(ob)
		//crush_object_part(ob, "stone")
		stone_crush_object_part(mut ob)
		
		println("> create sand")
		println("> (visual) create dust")
		println("> (audio) emit sound")
	} else {
		println("nothing happens")
	}
}
