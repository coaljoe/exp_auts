// XXX fixme?
local m_to_cm(m) = m * 100.0;
local cm_to_m(cm) = cm / 100.0;
local m_to_mm(m) = m * 1000.0;
local mm_to_m(mm) = mm / 1000.0;

{
  /*
  m_to_cm(m): m * 100.0,
  cm_to_m(cm): cm / 100.0,
  m_to_mm(m): m * 1000.0, 
  mm_to_m(mm): mm / 1000.0,
  */
  
  to_size(val, unit)::
    if unit == "mm" then mm_to_m(val)
    else if unit == "cm" then cm_to_m(val)
    else error "unknown unit",
}
