module mathex

import math as m

pub struct AABB {
pub:
	/*
	min_x f32
	min_y f32
	min_z f32
	max_x f32
	max_y f32
	max_z f32
	*/
	min [3]f32
	max [3]f32
}

pub fn new_aabb() AABB {
	return AABB{
		[3]f32{ init: m.inf(1) },
		[3]f32{ init: m.inf(-1) }}
}

pub fn new_aabb_from_points(min [3]f32, max[3]f32) AABB {
	return AABB{min, max}
}

// ContainsBox returns whether the bounding volume fully contains the volume
// defined by the given bounding box
pub fn (a &AABB) contains_box(ib AABB) bool {
	return a.contains_point(ib.min) && a.contains_point(ib.max)
}

// ContainsPoint returns whether the bounding volume contains the given point
pub fn (a &AABB) contains_point(ip [3]f32) bool {
	gt := ip[0] >= a.min[0] && ip[1] >= a.min[1] && ip[2] >= a.min[2]
	lt := ip[0] <= a.max[0] && ip[1] <= a.max[1] && ip[2] <= a.max[2]
	return gt && lt
}

fn (a &AABB) empty() bool {
	return (a.max[0] < a.min[0]) || (a.max[1] < a.min[1]) || (a.max[2] < a.min[2])
}

fn (a AABB) str() string {
	return "AABB<min: $a.min, max: $a.max>"
}
