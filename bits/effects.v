module bits

// Transmut effects
struct Effect {
mut:
	name string
	cond bool
	in_elem &Elem
	out_elem &Elem // Resulting element
	cond_temp int
}

fn new_effect() &Effect {
	e := &Effect{
		//in_elem: null,
		//out_elem: null,
		in_elem: &Elem(0),
		out_elem: &Elem(0),
	}
	return e
}
