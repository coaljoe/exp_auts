module main

struct Obj {
mut:
	name string
	x int
	y int

	// Object can be in the air ?
	z int
}

fn new_obj() &Obj {
	o := &Obj{}

	return o
}

fn (o &Obj) cx() int {
	return o.x / c_cell_w
}

fn (mut o Obj) set_cx(cx int) {
	//o.x = cx * c_cell_w
	//x1, y1 := cell_to_world_pos(cx, o.cy())
	x1, y1 := cell_to_world_pos_center(cx, o.cy())
	o.x = x1 // XXX fixme?
	o.y = y1
}

fn (o &Obj) cy() int {
	return o.y / c_cell_h
}

fn (mut o Obj) set_cy(cy int) {
	//o.y = cy * c_cell_h
	//x1, y1 := cell_to_world_pos(o.cx(), cy)
	x1, y1 := cell_to_world_pos_center(o.cx(), cy)
	o.x = x1 // XXX fixme?
	o.y = y1
}

// Approx. ?
fn (o &Obj) zlevel() int {
	return o.z / c_cell_zstep
}

// XXX
fn (mut o Obj) set_zlevel(v int) {
	o.z = v * c_cell_zstep
}
