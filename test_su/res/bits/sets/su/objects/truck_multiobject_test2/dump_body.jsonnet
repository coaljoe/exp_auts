{
  // dump body

  // weight: 2000 kg

  name: "dump_body",

//  info: {
//    // XXX
//    cat_id: "4032498f-747b-4b8f-87f7-5fa6cbfe09f7",
//    cat_name: "Test tire",
//    cat_description: "test tire",
//    cat_notes: 'tire size: 20"',
//    model_mass: 20,  // XXX?
//  },

  material: [

    {
      name: "dump_body",
      type: "from_mass",
      params: 2000,  // 2000 kg
      elem: "steel",
    },

  ],
}
