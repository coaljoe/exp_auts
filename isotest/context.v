module main

struct Context {
pub mut:
	app       &App
	drawer    &Drawer
	vars      &Vars
}

fn new_context() &Context {
	println("new_context()")

	c := &Context{
		app: &App(0),
		drawer: &Drawer(0),
		vars: &Vars(0),
	}

	return c
}

fn init_default_context() {
	println("init_default_context()")

	// XXX fixme?
	//ctx = new_context()
	ctx.vars = new_vars()
}

fn init_context() {
	// init all context?
}