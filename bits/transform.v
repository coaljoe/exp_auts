module bits

struct Transform {
mut:
	pos_x f32
	pos_y f32
	pos_z f32
	rot_x f32
	rot_y f32
	rot_z f32
}

fn new_transform() &Transform {
	t := &Transform{}
	return t
}
