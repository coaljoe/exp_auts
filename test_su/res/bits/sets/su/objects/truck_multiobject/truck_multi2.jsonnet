{
  // test truck
  // Tatra 148
  
  // weight: ~10,000 kg
  // num tires: 10 (3 axel)
  // tires: 20"
  // engine weight: 845 kg


  name: "truck",
  
  info: {
    // XXX
  	cat_id: "c63bd24b-43b4-4410-939d-b38e2b5e40b3",
  	cat_name: "Tatra 148",
    cat_description: "test truck",
    cat_notes: "",
    // XXX total model mass?
    model_mass: 10000,
  },
  
  material: [
  
  // Wheels -> Tires
  //wheel: import 'wheel_simple.jsonnet',
  //import 'wheel_simple2.libsonnet',
  //import "wheel_simple3.jsonnet",
  
  // Engine block:
  
  // Engine
  {
    name: "engine",
  	type: "from_mass",
  	params: 845, // 845 kg
  	elem: "steel",
  },
 
  // Transmission
  {
    name: "transmission",
  	type: "from_mass",
  	params: 300, // 300 kg
  	elem: "steel",
  },
  
  // The rest:
  {
    //name: "rest",
  	type: "from_rest_mass", // XXX
  	elem: "steel",
  },
  ],
  
  // XXX sub objects
  children: [
    {
      //skip: true,
      //name: "wheels", // XXX?
      path: "wheel.jsonnet",
      count: 10,
      //count: 0, // XXX bug
      // XXX do not fuse / leave as separate object
      distinct: false,
    },
    
    // XXX inline
    {
    
      // inline object
      object: {
        name: "test_wheel",
  
	    info: {
          // XXX
          cat_id: "test_wheel",
          cat_name: "Tatra 148 wheel",
          cat_description: "test wheel",
          cat_notes: "Tatra 148 or generic wheel(?); tire size: 20\"",
          model_mass: 20, // XXX?
        },
  
        material: [
  
          // Wheels -> Tires
          {
  	        name: "tires",
  	        type: "from_mass",
  	        params: 20, // 20 kg
  	        elem: "rubber",
          },
   
        ],
      },
    },
  ],
}
