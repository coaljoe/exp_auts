module main

import mathex
import isotest as it

fn main() {
	println("main()")

	if true {
		mut vo1 := it.new_voxel_obj(10, 10, 10)
		println("d1: ${vo1.data[0][0][0]}")
		vo1.data[0][0][0] = 1
		vo1.data[4][0][0] = 1
		println("d2: ${vo1.data[0][0][0]}")

		mut vo2 := new_voxel_obj(5, 10, 20)
		println("d3: ${vo2.data[0][0][0]}")
		vo2.data[4][9][19] = 1
		println("d4: ${vo2.data[0][0][0]}")

		mut vs1 := new_voxel_space(20, 20, 20)
		//n1 := new_voxel_obj_node_from_obj(0, 0, 0, vo1)
		n1 := new_voxel_obj_node_from_obj(1, 0, 0, vo1)
		vs1.nodes << n1

		//println("vs1.nodes: ${vs1.nodes}")

		v := vs1.is_empty(0, 0, 0)
		//vs1.is_empty(1, 0, 0)
		//vs1.is_empty(2, 0, 0)

		println("v: $v")

		box1 := mathex.new_aabb()

		println("box1: $box1")
	
		pp(2)
	}

	// XXX init context
	//mut ctx := new_context()
	ctx = new_context()
	init_default_context() // XXX fixme?
	//init_context()

	// XXX init app
	mut app := new_app()
	
	app.init(800, 600, "op_sim")

	for app.step() {
	}

	app.quit()

	println("done")
}
