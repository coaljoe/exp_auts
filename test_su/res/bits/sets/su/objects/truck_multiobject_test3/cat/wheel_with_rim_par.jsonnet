{

  parent: "wheel_with_rim",

  name: "wheel_with_rim_offroad",  // XXX ?

  info: {
    // XXX
    cat_id: "a5592d43-6459-4f09-95e1-c06c59bdf12e",
    cat_name: "Tatra 148 wheel / rim version (offroad)",
    cat_description: "test wheel with offroad rim",
    cat_notes: 'Tatra 148 or generic wheel(?); tire size: 20"',
    model_mass: 20,  // XXX?
  },

  material: [

    // Wheels -> Tires
    {
      parent.name: "tires", // XXX match unique?
      type: "from_mass",
      params: 25,  // 25 kg
      elem: "rubber",
    },
  ],
}
