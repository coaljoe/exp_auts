{
  // test house window
  // House window

  // width: 120 cm
  // height: 170 cm
  // mass: 80 kg


  name: "frame_window",

  cat: {
    cat_id: "010701f5-1026-4942-9343-7476847567aa",
    cat_name: "Frame window",
    cat_description: "test frame window",
    cat_notes: 'Test wooden window',
  }

  info: {
    model_mass: 80,  // XXX?
  },

  material: [

    // Frame
    {
      name: "frame",
      type: "from_mass",
      params: 70,  // 70 kg
      elem: "wood",
    },

    // Glass
    {
      name: "glass",
      type: "from_mass",
      params: 10,  // 10 kg, approx.
      elem: "glass",
    },
  ],
}
