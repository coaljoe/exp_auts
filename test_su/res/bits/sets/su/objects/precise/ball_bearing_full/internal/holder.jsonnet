{
  // test ball bearing ball holder

  name: "ball_bearing_holder",

  _info: {
    name: "ball bearing holder",
    description: "a ball berings holder",
    notes: "",

    //weight: "1 kg",
    //size: "70 cm",
	//length: "70 cm",
    //components: "balls, rings",

    materials: "metal",
    materials_long: "",
  },
  
  cat: {
    id: "14581308-1add-4020-d89b-c59818a236f3",
    name: "Ball bearing holder",
    description: "test ball bearing holder",
    notes: "",
  },

  materials: [
    {"metal", 0.1},
  ],
}
