{
  // test wheel with hub
  // Tatra 148 wheel with hub

  // weight tire: 20 kg
  // weight hub:
  // num tires: 10 (3 axel)
  // tires: 20"


  name: "wheel_with_hub",  // XXX ?

  info: {
    // XXX
    cat_id: "00211dae-a61b-415c-e53e-3031afd06685",
    cat_name: "Tatra 148 wheel / hub version",
    cat_description: "test wheel with hub",
    cat_notes: 'Tatra 148 or generic wheel(?); tire size: 20"',
    model_mass: 20,  // XXX?
  },

  material: [

    // Wheels -> Tires
    {
      name: "tires",
      type: "from_mass",
      params: 20,  // 20 kg
      elem: "rubber",
    },

    // Wheels -> Hubs
    {
      name: "hubs",
      type: "from_mass",
      params: 20,  // 20 kg
      elem: "steel",
    },
  ],
}
