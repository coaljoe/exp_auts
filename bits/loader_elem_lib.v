module bits

import json
//import os

struct ElemLibLoader {

}

// Elems.json
// Root
struct ElemLibFile {
	elems []ElemLibRec
}

// Elems.json
// Elem record
struct ElemLibRec {
	name string
	//elem_type ElemType
	elem_type_str string [json:elem_type] // XXX
	density int

	flameable bool
	liquid bool

	// XXX TODO
	//aggregate_state AggregateState
	
	notes string
}

pub fn new_elem_lib_loader() &ElemLibLoader {
	println("bits: new_elem_lib_loader()")

	l := &ElemLibLoader{}

	return l
}

pub fn (mut l ElemLibLoader) load(res_path string) {
	println("bits: ElemLibLoader load: res_path: $res_path")

	path := resolve_path(res_path) or {
		panic(err)
	}

	data := load_jsonnet_file_string(path)

	println("data:")
	println("$data")

	///*
	js := json.decode(ElemLibFile, data) or {
		println("json decode error:")
		panic(err)
		return
	}
	//*/

	println("recs:")
	println(js.elems)

	for rec in js.elems {
		println("loading rec.name '${rec.name}'...")
	
		mut el := new_elem()
		el.name = rec.name
		//el.elem_type = rec.elem_type
		el.elem_type = string_to_elem_type_enum(rec.elem_type_str)
		el.density = rec.density
		el.flameable = rec.flameable
		el.liquid = rec.liquid
		el.notes = rec.notes

		println("el: $el")

		// Add element
		bits_ctx.elem_lib[el.name] = el
	}

	println("bits: done ElemLibLoader load")
}
