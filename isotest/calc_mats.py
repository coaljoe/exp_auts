#!/bin/env python

import sys

if len(sys.argv) < 3:
    print("usage: %s mat_density (kg/m3) model_mass (kg)" % sys.argv[0])
    exit()

mat_density = int(sys.argv[1])

model_mass = float(sys.argv[2])

mat_volume = model_mass / mat_density
result_bits = mat_volume / (1.0/100) # 10x10x10

print("mat_density (kg/m3):", mat_density)
print("model_mass (kg):", model_mass)
print("")
print("raw mat_volume:", mat_volume)
print("")
print("result:")
print("mat_volume (m3):", f'{mat_volume:f}')
print("result_bits:", result_bits)
print("")
print("done")
