import bpy
import sys
import os

print("->", sys.argv)
argv = sys.argv
argv = argv[argv.index("--") + 1:]  # get all args after "--"

in_filename = os.path.basename(argv[0])
in_path = os.path.abspath(argv[0])
in_scene_name = argv[1]

print("in_filename:", in_filename)
print("in_path:", in_path)
print("in_scene_name:", in_scene_name)

filepath = in_filename
#directory = in_path + "\\Object\\"
#filename = "Cube"
directory = in_path + "\\Scene\\"
filename = in_scene_name

print("filepath:", filepath)
print("directory:", directory)
print("filename:", filename)


bpy.ops.wm.append(
    filepath=filepath,
    directory=directory,
    filename=filename)

#bpy.ops.wm.append(
#    filepath="box.blend",
#    directory="/home/k/dev/mrs/tools/object_renderer/testdata/box.blend\\Object\\",
#    filename="Cube")
