module bits

import debug { pp }

/*
//import lib
import debug
//import debug { pp }

//type pp = fn (debug.PPType)
//pp := debug.pp
//pp = debug.pp

__global (
	pp = debug.pp
	//pp = debug.xpp
)
*/

/*
//fn pp = debug.pp
//fn pp(v any) {
fn pp(v debug.PPType) {
	debug.pp(v)
}
*/

//type PPType = string | int

// XXX FIXME
fn pps(s string) {
	println(s)
	pp(2)
}

/*

//pub fn pp(v int) {
//fn pp(v int) {
//pub fn pp(v voidptr) {
pub fn pp(v PPType) {
//pub fn pp(v any) {

	println(v)
	println("panic: pp")
	println("")

	//println("bt:")
	println("Backtrace:")
	print_backtrace()
	//println("bt2:")
	//print_backtrace_skipping_top_frames(3)
	//println(v)

	//println("panic $v")
	
	panic("pp")
	//exit(v)
	//exit(2)
}

/*
pub fn pp(s string) {
	println(s)
	pp(2)
}
*/
*/