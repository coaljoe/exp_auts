module main

import sdl
import sdl.image as img

struct Drawer {
mut:
	w    int
	h    int
	font_name string
	font_size int
	
	color_white sdl.Color
	color_black sdl.Color
	color_green sdl.Color

	// Sdl
	font        voidptr
	renderer    voidptr
}

fn new_drawer() &Drawer {
	println("new_drawer()")

	d := &Drawer{
		font_name: "res/fonts/RobotoMono-Regular.ttf",
		font_size: 14,
		color_white: sdl.Color{byte(0xff), byte(0xff), byte(0xff), byte(0)},
		color_black: sdl.Color{byte(0x0), byte(0x0), byte(0x0), byte(0)},
		color_green: sdl.Color{byte(0x0), byte(0xff), byte(0x0), byte(0)},
	}

	// Update ctx
	ctx.drawer = d

	return d
}

fn (mut d Drawer) init(w int, h int, renderer voidptr) {
	// XXX fixme?
	//d.renderer = d.ctx.app.renderer
	d.w = w
	d.h = h
	d.renderer = renderer

	d.font = C.TTF_OpenFont(d.font_name.str, d.font_size)
}

// Load image as sdl tex
fn (mut d Drawer) load_image_sdl_tex(path string) voidptr {
	sdl_img := img.load(path.str)
	mut tex := voidptr(0)

	if !isnil(sdl_img) {
		tex = sdl.create_texture_from_surface(d.renderer, sdl_img)
	}

	return tex
}

fn (mut d Drawer) draw_image() {

}

fn (mut d Drawer) draw_sdl_tex(tex voidptr, x int, y int) {
	if isnil(tex) {
		println("tex is nil")
		pp(2)
		//return
	}
	texw := 0
	texh := 0
	C.SDL_QueryTexture(tex, 0, 0, &texw, &texh)
	//dstrect := sdl.Rect { (d.w / 2) - (texw / 2), 20, texw, texh }
	dstrect := sdl.Rect { x, y, texw, texh }
	// Currently we can't seem to use sdl.render_copy when we need to pass a nil pointer (eg: srcrect to be NULL)
	//sdl.render_copy(g.sdl.renderer, tv_logo, 0, &dstrect)
	C.SDL_RenderCopy(d.renderer, tex, voidptr(0), voidptr(&dstrect))
}

fn (mut d Drawer) draw_tex(tex &Texture, x int, y int) {
	d.draw_sdl_tex(tex.tex, x, y)
}

fn (mut d Drawer) draw_text(text string, x int, y int, c sdl.Color) {
	//println("drawer draw_text() text: $text, x: $x, y: $y, c: $c")

	tcol_ := C.SDL_Color{c.r, c.g, c.b, c.a}
	tsurf := C.TTF_RenderText_Solid(d.font, text.str, tcol_)
	ttext := C.SDL_CreateTextureFromSurface(d.renderer, tsurf)
	texw := 0
	texh := 0
	C.SDL_QueryTexture(ttext, 0, 0, &texw, &texh)
	dstrect := sdl.Rect { x, y, texw, texh }
	// sdl.render_copy(g.sdl.renderer, ttext, 0, &dstrect)
	C.SDL_RenderCopy(d.renderer, ttext, voidptr(0), voidptr(&dstrect))
	C.SDL_DestroyTexture(ttext)
	sdl.free_surface(tsurf)
}

fn (mut d Drawer) get_sdl_tex_size(tex voidptr) (int, int) {
	if isnil(tex) {
		println("tex is nil")
		pp(2)
		//return
	}
	texw := 0
	texh := 0
	C.SDL_QueryTexture(tex, 0, 0, &texw, &texh)

	return texw, texh
	
}

fn (mut d Drawer) clear() {
	C.SDL_RenderClear(d.renderer)
}
