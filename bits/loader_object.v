module bits

import json
import os
import x.json2
//import lib.jsonnet

struct ObjectLoader {

}

// Object.json
// Root
struct ObjectFile {
	name string // XXX object name is not optional?
	info ObjectInfo [optional]
	material []ObjectMaterialRec
	// Sub objects
	children []ObjectChildRec 
}

// XXX rename to ObjectInfoRec?
struct ObjectInfo {
	cat_id string [optional] //?
	cat_name string [optional]
	cat_description string [optional]
	cat_notes string [optional]
	model_mass f32
}

struct ObjectChildRec {
	// Rec type 1
	path string
	// Do not load rec
	skip bool [optinonal]
	// XXX hack: FIXME using default value to tell json.Null / no value
	//count int = -1 // XXX doesnt work
	count int [optional]
	distinct bool [optional]

	// Rec type 2 XXX fixme?
	object ObjectFile // XXX
}

// Object.json
// Material record
struct ObjectMaterialRec {
	name string [optional] // XXX fixme?
	typ string [json: 'type'] // XXX
	params_str string [raw; json: 'params']
	elem string
}

pub fn new_object_loader() &ObjectLoader {
	println("bits: new_object_loader()")

	l := &ObjectLoader{}

	return l
}

pub fn (mut l ObjectLoader) load(res_path string) &Object {
	println("bits: ObjectLoader load: res_path: $res_path")

	path := resolve_path(res_path) or {
		panic(err)
	}

	//data := load_json_file_string(path)
	data := load_jsonnet_file_string(path)

	println("data:")
	println("$data")

	///*
	js := json.decode(ObjectFile, data) or {
		println("json decode error:")
		panic(err)
		//return 
	}
	//*/

	// Create new object
	mut obj := new_object()

	println("material:")
	println(js.material)

	for i, rec in js.material {
		//println("loading rec.name '${rec.name}'...")
		println("loading rec i:'$i'...")

		println("rec: $rec")

		mut raw_p := json2.Any(json2.null)
		mut p := []json2.Any{}
		if rec.params_str != "" {
			raw_p = json2.raw_decode(rec.params_str) or {
				panic(err)
			}
			//q := raw_q.as_map()
			p = raw_p.arr()
		}
		

		/*
		println("p: $p")
		println("p0: ${p[0]}")
		z := p[0].arr()[0]
		println("z: $z")

		if p[0].arr().len > 1 {
			z2 := p[0].arr()[1]
			println("z2: $z2")
		}
		*/

		// Link
		elem := bits_ctx.elem_lib[rec.elem] 

		if rec.typ == "from_volume" {
			vol := p[0].f32()
			println("vol: $vol")
			
			obj.mat_info.add_elem_from_volume_name(rec.name, vol, elem)
			if rec.name != "" {
				//pt.name = rec.name
			}
		} else if rec.typ == "from_volume_cylinder" {
			h := size_from_json_tuple(p[0])
			d := size_from_json_tuple(p[1])

			println("h: $h")
			println("d: $d")

			obj.mat_info.add_elem_from_volume_cylinder_name(rec.name, h, d, elem)
			if rec.name != "" {
				//pt.name = rec.name
			}
		} else if rec.typ == "from_mass" {
			mass := p[0].f32()
			println("mass: $mass")
			
			obj.mat_info.add_elem_from_mass_name(rec.name, mass, elem)
			if rec.name != "" {
				//pt.name = rec.name
			}
		} else if rec.typ == "from_rest_mass" {
			println("from_rest_mass:")

			// XXX a special case
			// XXX TODO add check if this is the last block
			model_mass := js.info.model_mass
			cur_mass := obj.mass()
			
			mass := model_mass - cur_mass

			println("model_mass: $model_mass")
			println("cur_mass: $cur_mass")
			println("mass: $mass")

			//pp(2)
			
			obj.mat_info.add_elem_from_mass_name(rec.name, mass, elem)
		} else if rec.typ == "from_size" {
			size_x := size_from_json_tuple(p[0])
			size_y := size_from_json_tuple(p[1])
			size_z := size_from_json_tuple(p[2])
			//x := p[0].arr()
			//size_x := x[0].f32()
			//size_y := x[1].f32()
			//size_z := x[2].f32()
			println("size: $size_x, $size_y, $size_z")
			
			obj.mat_info.add_elem_from_size_name(rec.name, size_x, size_y, size_z, elem)
			if rec.name != "" {
				//pt.name = rec.name
			}
		} else {
			panic("unknown type: $rec.typ")
		}
	}

	println(obj.mat_info.elems_list())

	// Children

	if js.children.len > 0 {
		println("load children...")
		
		//println(js.children.len)
	}

	for _, rec in js.children {
		if rec.skip {
			println("warning: skip loading child record...")
			continue
		}
	
		mut loaded := false
	
		// XXX Rec type 1
		if rec.path != "" {

			println("rec: $rec")

			if rec.count < 0 {
				panic("negative count: $rec.count")
			}
			

			mut count := rec.count
			// XXX default bug workaround
			if rec.count == 0 {
				count = 1 // default count value, if not specified
			}

			mut nl := new_object_loader() // XXX

			//cur_dir := os.dir(path)
			//xpath := cur_dir + "/" + rec.path
			// XXX fixme?
			xpath := os.dir(res_path) + "/" + rec.path
			println("xpath: $xpath")

			for _ in 0..count {
				obj2 := nl.load(xpath)

				obj.add_child(obj2)

				if !rec.distinct {
					println("non distinct: fusing child...")
					obj.fuse_child(obj2)
				}
			}

			loaded = true
		}

		// XXX Rec type 2
		if rec.object.name != "" {
			loaded = true
		}

		if !loaded {
			panic("child not loaded")
		}
	}

	println("done load children")

	println("bits: done ObjectLoader load")

	return obj
}
