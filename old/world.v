module main

// World bit
//  size: 50x50x50cm (?)
struct Bit {
mut:
	elem_type ElemType
}

//fn (b Bit) str() string {
//	return "<Bit>"
//}

struct CompoundBit {
mut:
	elem1_type ElemType
	elem1_amt i8
	elem2_type ElemType
	elem2_amt i8
}

struct World {
mut:
	//bits [][][]Bit
	//bits [10][10][10]Bit
	bits [10][10]Bit // 2d (only surface bits)
}

fn new_world() &World {
	w := &World{}
	return w
}
