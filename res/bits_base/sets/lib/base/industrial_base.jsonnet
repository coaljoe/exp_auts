{
	// Industrial base
	elems: [
	{
		name: "concrete",
		elem_type: "concrete",
		density: 2400, // wiki
	},
	{
		name: "rubber",
		elem_type: "rubber",
		density: 1100, // https://www.engineeringtoolbox.com/density-solids-d_1265.html
	},
	{
		name: "steel",
		elem_type: "steel",
		density: 8000, // approx. average
	},
	{
		name: "glass",
		elem_type: "glass",
		density: 2500, // wiki
	},
	{
		name: "wood",
		elem_type: "wood",
		density: 700, // wiki
	},
    ],
}
