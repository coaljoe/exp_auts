module bits

import os
import regex
import x.json2
import lib.jsonnet

/*
pub fn enum_to_string(v) string {

}
*/

// XXX fixme: v bug #5181
pub fn string_to_elem_type_enum(s string) ElemType {
	//mut val := ElemType.none_

	val := match s {

		// XXX
		"none" { ElemType.none_ }
		//'none_' { ElemType.none_ }

		"wood" { ElemType.wood } 
		"sand" { ElemType.sand }
		"stone" { ElemType.stone }
		"coal" { ElemType.coal }
		"clay" { ElemType.clay }
		"iron" { ElemType.iron }
	
		"water" { ElemType.water }
		"oil" { ElemType.oil }
		"lava" { ElemType.lava }
	
		"vapour" { ElemType.vapour }
		"methane" { ElemType.methane }
	
		"soil" { ElemType.soil }
		"biomass" { ElemType.biomass }
		"silt" { ElemType.silt }
		"grass" { ElemType.grass }
	
		"natural_fiber" { ElemType.natural_fiber }	
		"natural_rubber" { ElemType.natural_rubber }
	
		"glass" { ElemType.glass }
		"concrete" { ElemType.concrete }
		"brick" { ElemType.brick }
		"steel" { ElemType.steel }
		"plastic" { ElemType.plastic }
		"cement" { ElemType.cement }
		"rubber" { ElemType.rubber }
		
		"textile" { ElemType.textile }

		"iron_ore" { ElemType.iron_ore }
		"aluminium_ore" { ElemType.aluminium_ore }
		"gold_ore" { ElemType.gold_ore }
		"silver_ore" { ElemType.silver_ore }
		"platinum_ore" { ElemType.platinum_ore }
		"copper_ore" { ElemType.copper_ore }
		"nickel_ore" { ElemType.nickel_ore }
		"zinc_ore" { ElemType.zinc_ore }
		"lead_ore" { ElemType.lead_ore }
		"uranium_ore" { ElemType.uranium_ore }
	
		"reinforced_concrete" { ElemType.reinforced_concrete }

		//else {}
		else { ElemType.none_ } // XXX FIXME
			//panic("bad enum")
		//}
	}

	return val
}

// Load jsonnet data as json string
pub fn load_jsonnet_file_string(path string) string {
	println("bits: load_jsonnet_file_string: path $path")

	res := jsonnet.load_file(path) or {
		panic(err)
	}

	return res
}

// Load and filter json data
pub fn load_json_file_string(path string) string {
	println("bits: load_json_file_string: path $path")

	data := os.read_file(path) or {
		println("file read error:")
		panic(err)
		//return
	}

	println("data: $data")

	//q := r"//.*?\n"
	//q := r"\/\/.*?\n"
	//q := r"//.*?"
	//q := "//.*\n"
	q := "(\/\/)([^\n\r]+)"
	//q := "\/\/.*?\n"
	//q := "//.*?\n"
	println("q: $q")


	///*
	mut re := regex.new()
	//mut re := regex.RE{}
	//re.debug = 1
	//re.flag = regex.F_BIN
	println("re.flag: ${re.flag}")

	re.compile_opt(q) or {
		println(err)
		panic(err)
	}
	//*/

	/*
	mut re := regex.regex_opt(q) or {
		println(err)
		panic(err)
	}
	*/

	//safe := ""
	//safe := reg.replace_all_string(data, "\n")
	safe := re.replace(data, "\n")
	//all := re.find_all(data)

	println("safe: $safe")
	//println("all: $all")

	//panic("2")

	return safe
}


// XXX try to convert size(?) tuple to normal value
// [1] -> [1]
// [1, "cm"] -> cm_to_m(1)
pub fn size_from_json_tuple(e json2.Any) f32 {
	//println("e: $e")

	x := e.arr()
	//x2 := x[0].arr()

	//println("x: $x")
	//println("x2: $x2")

	mut val := f32(0.0)

	if x.len > 1 {
		//println("derp")

		// Try to decode tuple
		amt := x[0].f32()
		kind := x[1].str()
		
		//println("amt: $amt, kind: $kind")

		if kind == "cm" {
			val = cm_to_m(amt)
		} else if kind == "mm" {
			val = mm_to_m(amt)
		} else {
			panic("unknown kind: $kind")
		}
	} else {
		val = x[0].f32()
	}

	//println("val = $val")
	//panic("2")

	return val
}
