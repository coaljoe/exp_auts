{
  // test truck
  // Tatra 148

  // weight: ~10,000 kg
  // num tires: 10 (3 axel)
  // tires: 20"
  // engine weight: 845 kg


  name: "truck",

  info: {
    // XXX
    cat_id: "c63bd24b-43b4-4410-939d-b38e2b5e40b3",
    cat_name: "Tatra 148",
    cat_description: "test truck",
    cat_notes: "",
    // XXX total model mass?
    model_mass: 10000,
  },

  material: [

    // Wheels -> Tires

    // Engine block:

    // Engine
    {
      name: "engine",
      type: "from_mass",
      params: 845,  // 845 kg
      elem: "steel",
    },

    // Transmission
    {
      name: "transmission",
      type: "from_mass",
      params: 300,  // 300 kg
      elem: "steel",
    },

    // The rest:
    {
      //name: "rest",
      type: "from_rest_mass",  // XXX
      elem: "steel",
    },
  ],

  // // XXX sub objects
  // children: [
  //  {
  //    //skip: true,
  //    //name: "wheels", // XXX?
  //    //path: "wheel.jsonnet",
  //    path: "wheel_with_rim.jsonnet",
  //    count: 10,
  //    distinct: false,
  //  },

  ],
}
