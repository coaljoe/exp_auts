{
  // test wheel with rim
  // Tatra 148 wheel with rim

  // weight tire: 20 kg
  // weight rim: ~30 kg
  // num tires: 10 (3 axel)
  // tires: 20"


  name: "wheel_with_rim",  // XXX ?

  info: {
    // XXX
    cat_id: "00211dae-a61b-415c-e53e-3031afd06685",
    cat_name: "Tatra 148 wheel / rim version",
    cat_description: "test wheel with rim",
    cat_notes: 'Tatra 148 or generic wheel(?); tire size: 20"',
    model_mass: 20,  // XXX?
  },

  material: [

    // Wheels -> Tires
    {
      name: "tires",
      type: "from_mass",
      params: 20,  // 20 kg
      elem: "rubber",
    },

    // Wheels -> Rims
    {
      name: "rims",
      type: "from_mass",
      params: 30,  // 30 kg, approx.
      elem: "steel",
    },
  ],
}
