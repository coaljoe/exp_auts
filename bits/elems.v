module bits

enum ElemType {
	none_ // None/Empty
	
	//// Primary form
	wood sand stone coal clay iron
	// rock -> stone (?)
	// Liquid
	water oil lava
	// Gas(?)
	vapour methane
	// Extra(?) / Compounds
	soil biomass silt grass
	// Organic
	natural_fiber // fiber_crops
	natural_rubber // ??
	
	//// Secondary form
	glass concrete brick steel plastic cement
	rubber // ??
	// Liquid
	// XXX simplify: no need for special-state concrete,
	// use basic type
	//liquid_concrete,
	// Organic
	textile

	//// Compounds(?)
	// Ores
	iron_ore
	aluminium_ore // Bauxite
	gold_ore
	silver_ore
	platinum_ore // (?)
	copper_ore
	nickel_ore // Garnierite
	zinc_ore
	lead_ore
	uranium_ore
	//alumina, // Aluminium oxide
	// Construction
	// XXX add only as a subtype of concrete? only useful for recycling information...
	
	reinforced_concrete // Approx 90% concrete, 10% steel (on average)
}

enum SolidState {
	solid
	liquid
	gas
}

enum AggregateState {
	solid
	crushed
}

[heap]
struct Elem {
mut:
	name string
	elem_type ElemType
	density int // kg/m3 // XXX per bit?
	// XXX TODO: rounding precision: different precision
	// for different elements (more crude means less precision for volume calculations)
	prec int
	flameable bool
	liquid bool // XXX is technically a liquid in its normal state?

	// Matter state
	solid_state SolidState

	// Material aggregate state
	// XXX?
	aggregate_state AggregateState

	notes string

	// when
	//when_crushed_fn fn()
	//when_crushed_effects []Effect
	//when_crushed_out_elems []Elem

	// list of processors
	//generic_processors []GenericProcessorI // XXX no interface arrays
	//generic_processors map[string]GenericProcessorI
	//generic_processor0 &GenericProcessorI
	//generic_processor0 &GenericCrusherProcessor


	// XXX
	//crush_object_part(ob &Object)

	//processor_factories []GenericProcessorI
}

fn new_elem() &Elem {
	/*
	e := &Elem{
		//when_crushed_effects: []Effect{},
		//when_crushed_out_elems: []Elem{},
		//generic_processor0: none,
	}
	*/
	e := &Elem{
		solid_state: .solid,
		aggregate_state: .solid,
		//prec: 4, // XXX what is default precision?
	}
	
	return e
}
