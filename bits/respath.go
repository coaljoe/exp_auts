package rx

type ResPath struct {
	// Res Paths
	Paths []string
}

// Shortcut
func resolvePath(resPath string) (bool, string) {
	return conf.ResPath.ResolvePath(resPath)
}

func NewResPath() *ResPath {
	println("NewResPath")

	rp := &ResPath{
		Paths: make([]string, 0),
	}
	
	return rp
}

func (rp *ResPath) addPath(path string, insert bool) {
	if rp.HasPath(path) {
		pp("ResPaths already have path: path:", path)
	}

	if path == "" {
		pp("path can't be empty: path:", path)
	}

	if !insert {
		rp.Paths = append(rp.Paths, path)
	} else {
		i := 0
		a := rp.Paths
		a = append(a[:i], append([]string{path}, a[i:]...)...)
	}
}

func (rp *ResPath) AddPath(path string) {
	println("ResPath.AddPath path:", path)

	if rp.HasPath(path) {
		pp("ResPaths already have path: path:", path)
	}

	rp.Paths = append(rp.Paths, path)
}

func (rp *ResPath) InsertPath(path string) {
	println("ResPath.InsertPath path:", path)
	
	rp.addPath(path, true)
}

func (rp *ResPath) RemovePath(path string) {
	println("ResPath.RemovePath path:", path)

	if !rp.HasPath(path) {
		pp("no such path in ResPaths: path:", path)
	}

	for idx, rpath := range rp.Paths {
		if rpath == path {
			 // Delete
			 i := idx
			 rp.Paths = append(rp.Paths[:i], rp.Paths[i+1:]...)
			 break
		}
	}
}

func (rp *ResPath) HasPath(path string) bool {
	for _, rpath := range rp.Paths {
		if rpath == path {
			return true
		}
	}
	return false
}

// Return the topmost occurrence of path
func (rp *ResPath) ResolvePath(path string) (bool, string) {
	println("ResPath.ResolvePath path:", path)

	ok := false

	resolvedPath := ""
	found := false
	for _, rpath := range rp.Paths {
		xpath := rpath + "/" + path
		ok2, err := exists(xpath)
		if err != nil {
			pp(err)
		}
		if ok2 {
			found = true
			resolvedPath = xpath
			break
		}
	}

	if found {
		ok = true
	}

	println("ok:", ok)
	println("resolvedPath:", resolvedPath)

	return ok, resolvedPath
}

func (rp *ResPath) PrintPaths() {
	println("ResPaths:")
	for _, rpath := range rp.Paths {
		println("  " + rpath)
	}
	println("End ResPaths")
}
