module main

import os
//import strconv

struct FieldView {
mut:
	m &Field
	tiles map[string]&Texture
	tile_names []string // tile_id -> tile_name

	tile_bg map[string]&Texture
}

fn new_fieldview(m &Field) &FieldView {
	v := &FieldView{
		m: m,
	}

	return v
}

fn (mut v FieldView) load() {
	println("FieldView load()")

	if isnil(ctx) {
		println("ctx is nil")
		pp(2)
	}

	// Load tiles
	
	//path := "res/images/tiles/test1"
	path := v.m.tiles_path

	println("load tiles...")

	mut tile_idx := 0
	for f in os.walk_ext(path, ".png") {
	//for f in os.ls(path) or {} {

		// Skip subdirs
		if os.dir(f) != path {
			println("skipping $f")
			continue
		}

		println("-> f: $f")
		//println(os.dir(f))

		//mut name := os.file_name(f).trim_right(".png")
		mut name := os.file_name(f).all_before_last(".png")
		//println("z: $name")
		if name.count("_") != 1 {
			//idx := name.index_after(5)
			name = name.all_before_last("_")
		}
		println("name: $name")

		//tex := ctx.drawer.load_image_tex(f)
		mut tex := new_texture()
		tex.load(f)

		v.tiles[name] = tex

		v.tile_names << name
		tile_idx++

		//pp(2)
	}

	//pp(2)

	// Load tile bgs

	if true {
		mut tex2 := new_texture()
		//f := path + "/bg/test1/image_0.png"
		f := path + "/bg/test2/image_0.png"
		tex2.load(f)

		v.tile_bg["test1"] = tex2
	}
}

// Draw tile at position (tex version)
[inline]
fn (mut v FieldView) draw_tile_tex(tex &Texture, px int, py int) {
	
	mut sx, mut sy := world_to_screen_pos(px, py)

	// XXX apply half-tile yshift for 32x32 tiles
	sy -= c_cell_h
	

	//println("sx: $sx")
	//println("sy: $sx")

	tex.draw_pos(sx, sy)
	//tex.draw_pos(px, py)
}

// Draw tile at position
// XXX draw with top-left corner of 32x16 tile
// XXX add draw_tile_center function to drawing tiles at center position
[inline]
fn (mut v FieldView) draw_tile(tile_id int, px int, py int) {

	//name := "tile_${tile_id}"	// XXX leak		
	//name := "tile_"	+ tile_id.str()

	//tmp_str := tile_id.str()
	//name := "tile_"	+ tmp_str
	//println("name: $name")
	//free(tmp_str)

	//tmp_str := strconv.v_sprintf("%d", tile_id)
	//tmp_str := strconv.v_sprintf("test")

	/*
	a := "World"
	s := strconv.v_sprintf("Hello %s!", a)
	println(s)
	*/

	name := v.tile_names[tile_id]
	
	mut tex := v.tiles[name]

	
	if isnil(tex) {
		println("tex is nil")
		pp(2)
	}
	
	v.draw_tile_tex(tex, px, py)
}

// Draw tile bg at position
[inline]
//fn (mut v FieldView) draw_tile_bg(tile_id int, px int, py int, y_offset int) {
fn (mut v FieldView) draw_tile_bg(tile_id int, px int, py int) {

	//name := v.tile_[tile_id]
	
	//mut tex := v.tiles[name]

	// XXX
	mut tex := v.tile_bg["test1"]

	
	if isnil(tex) {
		println("tex is nil")
		pp(2)
	}
	
	
	mut sx, mut sy := world_to_screen_pos(px, py)

	// XXX apply half-tile yshift for 32x32 tiles
	sy -= c_cell_h

	// XXX apply extra horizontal shift for larger sprites, fixme ? not tested.
	if true {
		if tex.w > c_cell_w {
			coeff := (c_cell_w - tex.w) / 2
			sx += coeff

			//println("coeff: $coeff")
			//pp(2)
		}
	}

	//println("sx: $sx")
	//println("sy: $sx")

	tex.draw_pos(sx, sy)

	//v.draw_tile(tile_id, px, py)
}

// XXX internal ?
[inline]
fn (mut v FieldView) do_draw_tile_cell_pos(tile_id int, tex &Texture, cx int, cy int, y_offset int, draw_bg bool) {
	x := cx
	y := cy

	// ? Staggered coords
	if false {
		mut shx := 0
		if y % 2 != 0 {
			shx = c_cell_w / 2
		}

		mut px := x * c_cell_w + shx
		mut py := y * c_cell_h / 2
		
		// XXX
		// y-inv ?
		py += (-y_offset)
	}

	//if cx > 20 || cy > 0 {
	//	return	
	//}
	
	//if cx > 20 || cy > 10 {
	//	return	
	//}

	// ? Non-staggered
	/*
	mut shx := -(x * c_cell_w/2)
	mut shy := x * c_cell_h/2
	mut px := x * c_cell_w + shx
	mut py := y * c_cell_h + shy
	*/
	//  screen.x = offsetX - (y * TILE_WIDTH/2) + (x * TILE_WIDTH/2) - (TILE_WIDTH/2);
    //  screen.y = offsetY + (y * TILE_DEPTH/2) + (x * TILE_DEPTH/2);

	mut shx := 0
	mut shy := 0
    
	mut px := shx - (y * c_cell_w/2) + (x * c_cell_w/2) - (c_cell_w/2)
	mut py := shy + (y * c_cell_h/2) + (x * c_cell_h/2)

	// XXX
	// y-inv ?
	py += (-y_offset)

	if draw_bg {
		bg_py := py + c_cell_h

		//v.draw_tile_bg(0, px, py)
		v.draw_tile_bg(0, px, bg_py)
	}

	if tile_id >= 0 {
		v.draw_tile(tile_id, px, py)
	} else {
		// XXX tex version ?
		v.draw_tile_tex(tex, px, py)
	}

	/*
	// XXX
	// y-inv ?
	py += (-y_offset)

	v.draw_tile(tile_id, px, py)
	*/

	/*
	if y_offset == 0 {
		v.draw_tile(tile_id, px, py)
	} else {
		// XXX
		// y-inv ?
		py += (-y_offset)

		bg_py := py + c_cell_h

		//v.draw_tile_bg(0, px, py)
		v.draw_tile_bg(0, px, bg_py)

		v.draw_tile(tile_id, px, py)
	}
	*/
}

// Draw tile at cell position
fn (mut v FieldView) draw_tile_cell_pos(tile_id int, cx int, cy int, y_offset int) {
	v.do_draw_tile_cell_pos(tile_id, unsafe { 0 }, cx, cy, y_offset, false)
}

// Draw tile at cell position (tex version?)
fn (mut v FieldView) draw_tile_cell_pos_tex(tex &Texture, cx int, cy int, y_offset int) {
	v.do_draw_tile_cell_pos(-1, tex, cx, cy, y_offset, false)
}

// Draw tile at cell position (with bg)
fn (mut v FieldView) draw_tile_cell_pos_with_bg(tile_id int, cx int, cy int, y_offset int) {
	v.do_draw_tile_cell_pos(tile_id, unsafe { 0 }, cx, cy, y_offset, true)
}

// Draw tile at cell position (with z)
[inline]
fn (mut v FieldView) draw_tile_cell_pos_z(tile_id int, cx int, cy int, cz int) {

	if cz == 0 {
		v.draw_tile_cell_pos(tile_id, cx, cy, 0)
	} else {
		// Draw bg
		yoffset := cz * c_cell_zstep

		//v.draw_tile_bg(0, cx, cy, yoffset)
		//v.draw_tile_cell_pos(tile_id, cx, cy, yoffset)
		v.draw_tile_cell_pos_with_bg(tile_id, cx, cy, yoffset)
	}
}

// XXX Cell.draw
// TODO add draw_cell_at ?
//[inline]
fn (mut v FieldView) draw_cell(c &Cell) {
	cx := c.x
	cy := c.y
	cz := c.z

	//v.draw_tile_cell_pos(c.tile_id, cx, cy, 0)
	v.draw_tile_cell_pos_z(c.tile_id, cx, cy, cz)

	// XXX Draw cell's wall
	if !isnil(c.wall) && !isnil(c.wall.view) {
		//w := c.wall
		mut spr := c.wall.view.spr

		//pp(2)

		spr.force_draw()
	}

	// XXX Draw cell's wall
	if !isnil(c.map_obj) {
		//w := c.wall
		mut spr := c.map_obj.view.spr
		//pp(2)
		
		spr.force_draw()
	}
}

fn (mut v FieldView) spawn() {
	println("fiedview spawn()")
	
	v.load()
}

fn (mut v FieldView) draw() {
	println("fieldview draw()")

	for y in 0 .. v.m.h {
		for x in 0 .. v.m.w {
			//c := v.m.cells[x][y]
			c := &v.m.cells[x][y] // XXX ref

			//mut px := c_cell_w * x
			//mut py := c_cell_h * y

			//v.draw_tile_cell_pos(c.tile_id, x, y)
			//pp(2)

			v.draw_cell(c)
		
			//v.m.ctx.drawer.draw_tex(px, py, tex)
			//ctx.drawer.draw_tex(tex, px, py)
			//tex.draw_pos(px, py)
			//tex.draw_pos(sx, sy)
		}
	}
}
