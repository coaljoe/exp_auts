{
  // test ball bearing ball outer ring

  name: "ball_bearing_outer_ring",

  _info: {
    name: "ball bearing outer ring",
    description: "a ball berings outer ring",
    notes: "",

    //weight: "1 kg",
    //size: "70 cm",
	//length: "70 cm",
    //components: "balls, rings",

    materials: "metal",
    materials_long: "",
  },
  
  cat: {
    id: "14581308-1add-4020-d89b-c59818a236f3",
    name: "Ball bearing outer ring",
    description: "test ball bearing outer ring",
    notes: "",
  },

  materials: [
    {"metal", 1.0},
  ],
}
