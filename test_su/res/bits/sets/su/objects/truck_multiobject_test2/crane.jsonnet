{
  // truck crane
  
  // XXX currently not in catalogue for simplicity

  // AD 20 CKD
  // load weight: 16,000 kg (20,000 kg)
  // weight: 8000 kg

  name: "truck_crane",

//  info: {
//    // XXX
//    cat_id: "4032498f-747b-4b8f-87f7-5fa6cbfe09f7",
//    cat_name: "Test tire",
//    cat_description: "test tire",
//    cat_notes: 'tire size: 20"',
//    model_mass: 20,  // XXX?
//  },

  material: [

    {
      name: "truck_crane",
      type: "from_mass",
      params: 8000,  // 8000 kg
      elem: "steel",
    },

  ],
}
