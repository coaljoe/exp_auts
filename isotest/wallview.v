module main

struct WallView {
mut:
	m &Wall
	spr &Sprite

	spawned bool 
}

fn new_wallview(m &Wall) &WallView {
	println("new_wallview()")

	v := &WallView{
		m: m,
		spr: &Sprite(0),
	}

	return v
}

fn (mut v WallView) load() {
	
}

fn (mut v WallView) build() {
	println("WallView biuld()")

	//v.spr.load("res/objects/walls/wall3/image_0.png")
	path := "res/objects/walls/wall3/image_${v.m.image_variant}.png"
	v.spr.load(path)
}

fn (mut v WallView) spawn() {
	println("WallView spawn()")

	v.load()

	v.spr = new_sprite()
	//v.spr.load("res/objects/walls/wall1/image_0.png")
	//v.spr.load("res/objects/walls/wall3/image_0.png")

	// XXX ?
	v.build()

	v.spr.visible = false
	v.spr.spawn()

	v.spawned = true
}

fn (mut v WallView) draw() {
	println("WallView draw()")

	if !v.spawned {
		return
	}

	// XXX fixme
	v.update(0)

	println("x: $v.spr.x, y: $v.spr.y")
	//pp(2)

	/*
	//x1, y1 := cell_to_world_pos_center(4, 4)
	x1, y1 := cell_to_world_pos_center(0, 0)
	//println("x1: $x1, y1: $y1")
	//pp(2)
	v.spr.x = x1
	v.spr.y = y1
	*/

	//v.spr.draw()
}

fn (mut v WallView) update(dt f32) {
	v.spr.x = v.m.x
	//v.spr.y = v.m.y

	// XXX
	py := v.m.y - v.m.z
	v.spr.y = py
}
