module main

// Game/GameState
struct Game {
mut:
	field     &Field
	walltool  &WallTool
	hud       &Hud
	spritesys &SpriteSys
	viewport  &Viewport
}

fn init_game() {
	println("init_game()")

	mut g := &Game{
		field: new_field(),
		walltool: new_walltool(),
		hud: new_hud(),
		spritesys: new_spritesys(),
		viewport: new_viewport(),
	}

	// XXX fixme ?
	g.init()

	game = g
}

// XXX fixme ?
fn (mut g Game) init() {
	println("Game init")

	g.hud.init()
}

fn (mut g Game) start() {
	println("Game start")

	//g.hud.spawn()
	g.field.spawn()
}

fn (mut g Game) draw() {
	g.field.draw()
	g.spritesys.draw()
	g.hud.draw()
}

fn (mut g Game) update(dt f32) {
	g.field.update(dt)
	g.spritesys.update(dt)
	g.hud.update(dt)
	g.viewport.update(dt)
}
