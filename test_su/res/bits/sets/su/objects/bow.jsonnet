local util = import "util.libsonnet";

{
  // medieval bow
  // 70cm

  "name": "bow",
  "material": [
  // Shaft
  {
  	"type": "from_volume",
  	"params": [0.88],
  	"elem": "wood"
  },
  // Bowstring
  // length: 70cm
  {
  	"type": "from_volume_cylinder",
  	"params": [2.8, util.to_size(0.7*4, "mm")],
  	
  	// XXX extended exposed API use (?)
  	// like bits.ext (?)
  	// needs to be evaluated with another/custom binary?
  	// like bits/bin/conf_data_eval.bin (?)
  	// ext is extension (?)
  	//"params": [2.8, ext.to_size(0.7*4, "mm")],
  	
  	//"params": [2.8, to_size(0.7*4, "mm")],
  	//"params": [2.8, to_size(4, "mm")], // 0.7*4; 4mm
  	"elem": "textile"
  }
  ]
}
