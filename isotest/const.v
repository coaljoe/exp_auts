module main

const (
	c_cell_w = 32
	c_cell_h = 16

	// Tile high step
	//c_cell_zstep = 16
	c_cell_zstep = 8
	//c_cell_zstep = 4
	//c_cell_zstep = 2
	//c_cell_zstep = 1

	// XXX
	//c_cell_max_zsteps = 10
	c_cell_max_zsteps = 15
)
