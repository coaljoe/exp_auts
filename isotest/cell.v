module main

struct Cell {
mut:
	// XXX pos
	x int
	y int

	// XXX z level ?
	z int
	tile_id int
	open bool

	wall &Wall // XXX use wall_id?
	map_obj &MapObj
}

fn new_cell() Cell {
	c := Cell{
		open: true,
		wall: &Wall(0),
		map_obj: &MapObj(0),
	}
	
	return c
}

fn (c &Cell) draw() {
	println("panic: not implemented")
	pp(1)
}
