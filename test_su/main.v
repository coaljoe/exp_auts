module main

//import flag
import os

import debug { pp }
import bits

fn test_mywall() {
	println("test_mywall()")

	// Wall is 1x1x6 m in size

	// Create wall

	mut w := bits.new_object()
	w.name = "wall"

	println(isnil(bits_ctx))
	//println(bits_ctx.elem_lib)
	println(bits_ctx.elem_lib["water"])
	//println(bits_ctx.elem_lib["stone"])

	//w.mat_info.add_elem_from_size(1, 1, 6, bits_ctx.elem_lib["stone"])
	//w.mat_info.add_elem_from_size(1, 1, 6, bits_ctx.elem_lib["concrete"])
	//w.mat_info.add_elem_from_size(1, 1, 6, bits.bits_ctx.elem_lib["stone"])
	w.mat_info.add_elem_from_size(1, 1, 6, bits.ctx().elem_lib["concrete"])


	println("w new bits volume: ${w.bits_volume()}")
	println("w new volume: ${w.volume()}")

	println("done test_mywall")
}

fn test_object_loader() {
	println("test_object_loader")

	mut l2 := bits.new_object_loader()
	//l2.load("res/bits/sets/me/objects/wall.json")
	//l2.load("res/bits/sets/me/objects/bow.json")
	//l2.load("sets/su/objects/bow.json")
	obj := l2.load("sets/su/objects/truck.jsonnet")
	//l2.load("res/bits/sets/me/objects/arrow.json")

	println("obj volume: ${obj.volume()}")
	println("obj mass: ${obj.mass()}")
	println("obj parts_meta: ${obj.mat_info.parts_meta}")

	println("done test_object_loader")
}

fn test_object_loader2() {
	println("test_object_loader2")

	mut l2 := bits.new_object_loader()
	//mut obj := l2.load("sets/su/objects/truck_multiobject/truck_multi2.jsonnet")
	//obj2 := l2.load("sets/su/objects/truck_multiobject/wheel.jsonnet")

	// truck_multiobject2
	mut obj := l2.load("sets/su/objects/truck_multiobject2/truck.jsonnet")
	obj2 := l2.load("sets/su/objects/truck_multiobject2/wheel.jsonnet")

	//obj.children << obj2

	//obj.add_child(obj2)

	/*
	// XXX
	obj.add_child(obj2) or {
		panic(err)
	}
	obj.add_child(obj2) or {
		panic(err)
	}
	*/

	//obj.fuse_children()

	println("obj2 volume: ${obj2.volume()}")
	println("obj2 mass: ${obj2.mass()}")

	println("obj volume: ${obj.volume()}")
	println("obj mass: ${obj.mass()}")
	println("obj mass_rec: ${obj.mass_rec()}")
	println("obj mat_info.elems: ${obj.mat_info.elems_list()}")
	println("obj parts_meta: ${obj.mat_info.parts_meta}")

	println("obj num_children: ${obj.num_children()}")

	println("done test_object_loader2")
}

fn test_proc() {
	println("test_proc()")

	// Wall is 1x1x6 m in size

	// Create wall

	mut w := bits.new_object()
	w.name = "wall"

	println("z")

	println(isnil(bits_ctx))
	//println(bits_ctx.elem_lib)
	println(bits_ctx.elem_lib["raw_wood"])
	//println(bits_ctx.elem_lib["stone"])

	pp("z2")

	//w.mat_info.add_elem_from_size(1, 1, 6, bits_ctx.elem_lib["stone"])
	//w.mat_info.add_elem_from_size(1, 1, 6, bits_ctx.elem_lib["concrete"])
	//w.mat_info.add_elem_from_size(1, 1, 6, bits.bits_ctx.elem_lib["stone"])
	w.mat_info.add_elem_from_size(1, 1, 6, bits.ctx().elem_lib["concrete"])


	println("w new bits volume: ${w.bits_volume()}")
	println("w new volume: ${w.volume()}")

	println("done test_proc")
}


fn main() {
	println("main()")

	//bits.init_bits(args: os.args)

	//opts := bits.new_default_bits_opts()
	mut opts := bits.new_bits_opts()
	opts.res_paths << "res/mybits"

	opts.args = os.args
	
	//bits.init_bits(res_args: args: os.args)
	bits.init_bits(opts)

	//pp(3)

	//test_mywall()

	//test_object_loader()
	//test_object_loader2()

	test_proc()

	println("done")
}
