module main

struct VoxelObjNode {
// Position / Node data
// Todo: move node data to separate struct ?
pub mut:
	x int
	y int
	z int

	visible bool

// VoxelObj
	obj &VoxelObj
}

// XXX rename to new_voxel_obj_node_from_obj_pos ?
fn new_voxel_obj_node_from_obj(x int, y int, z int, obj &VoxelObj) &VoxelObjNode {
	von := &VoxelObjNode {
		x: x,
		y: y,
		z: z,

		visible: true,

		obj: obj,
	}

	return von
}

struct VoxelSpace {
pub:
	dx int
	dy int
	dz int

mut:
	//objs []&VoxelObj
	nodes []&VoxelObjNode
}

fn new_voxel_space(dx int, dy int, dz int) &VoxelSpace {
	println("new_voxel_space dx: $dx, dy: $dy, dz: $dz")

	vs := &VoxelSpace{
		dx: dx,
		dy: dy,
		dz: dz,

		//nodes: []&VoxelObjNode{0},
	}

	return vs
}

// Is empty position / voxel in space 
// TODO: add get_value_at(x, y, z) u8
fn (vs &VoxelSpace) is_empty(x int, y int, z int) bool {
	println("VoxelSpace is_empty x: $x, y: $y, z: $z")

	// XXX ?
	//mut v := -1
	// By default its empty ?
	mut v := 0

	for n in vs.nodes {
		println("->")
	
		// Calculate coordinates basid on offset / base
		// model local coords ?
		// world to local / model ?
		local_cx := x - n.x
		local_cy := y - n.y
		local_cz := z - n.z

		println("local_cx: $local_cx")
		println("local_cy: $local_cy")
		println("local_cz: $local_cz")

		//pp(3)

		// XXX skip if model is ouside of coords
		if local_cx < 0 || local_cy < 0 || local_cz < 0 {
			println("skip...")
			continue
		}

		v = n.obj.data[local_cx][local_cy][local_cz]
		println("new v: $v")
		break
	}

	if v == 0 {
		return true
	}

	return false
}
