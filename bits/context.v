module bits

struct Context {
pub mut:
	// Resource root. (res/bits)
	//res_path string
	res_path &ResPath
	
	//sets_path string
	set_name string
	set_path string

	elem_lib map[string]&Elem
}

fn new_context() &Context {
	println("bits: new_context()")

	c := &Context{
		res_path: new_res_path(),
	}

	return c
}
