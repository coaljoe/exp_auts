module main

import mathex

// XXX a voxel model ?
[heap]
struct VoxelObj {
pub:
	dx int
	dy int
	dz int

	aabb mathex.AABB
mut:
	// Bit data ?
	data [][][]u8
}

fn new_voxel_obj(dx int, dy int, dz int) &VoxelObj {
	println("new_voxel_obj dx: $dx, dy: $dy, dz: $dz")

	mut vo := &VoxelObj{
		dx: dx,
		dy: dy,
		dz: dz,

		// ?
		//aabb: mathex.new_aabb(),
		aabb: mathex.new_aabb_from_points([f32(0.0), 0.0, 0.0]!, [f32(dx), f32(dy), f32(dz)]!),
	}

	//vo.data = [][][]u8{len: dz, init: [][]u8{len: dy, init: []u8{len: dx}}}
	vo.data = [][][]u8{len: dx, init: [][]u8{len: dy, init: []u8{len: dz}}}


	//// Test bits ?
	for z in 0..vo.dz {
		for y in 0..vo.dy {
			for x in 0..vo.dx {
				//println("-> $z, $y, $x")
				vo.data[x][y][z] = 0
				//vo.data[z][x][y] = 0
			}
		}
	}

	// XXX
	vo.fill(1)

	println("done new_voxel_obj")

	return vo
}

// XXX fill data with value
fn (mut vo VoxelObj) fill(v u8) {
	println("VoxelObj fill v: $v")

	for z in 0..vo.dz {
		for y in 0..vo.dy {
			for x in 0..vo.dx {
				vo.data[x][y][z] = v
			}
		}
	}
}

// Clear model data
fn (mut vo VoxelObj) clear() {
	println("VoxelObj clear")
	
	vo.fill(0)
}
