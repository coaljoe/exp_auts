{
  // test ball bearing ball

  name: "ball_bearing_ball",

  _info: {
    name: "ball bearing ball",
    description: "a 40mm ball",
    notes: "",

    //weight: "1 kg",
    //size: "70 cm",
	//length: "70 cm",
    //components: "balls, rings",

    materials: "metal",
    materials_long: "",
  },
  
  cat: {
    id: "14581308-1add-4020-d89b-c59818a236f3",
    name: "Ball bearing ball",
    description: "test ball bearing ball",
    notes: "",
  },

  materials: [
    // Balls
    {"metall", 0.5},
  ],
}
