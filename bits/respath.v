module bits

import os

pub struct ResPath {
pub mut:
	// Res Paths
	paths []string
}

// Shortcut
//pub fn resolve_path(res_path string) (bool, string) {
pub fn resolve_path(res_path string) ?string {
	return bits_ctx.res_path.resolve_path(res_path)
}

pub fn new_res_path() &ResPath {
	println("bits: new_res_path()")

	rp := &ResPath{
	}
	
	return rp
}

fn (mut rp ResPath) do_add_path(path string, insert bool) {
	if rp.has_path(path) {
		pps("ResPaths already have path: path: $path")
	}

	if path == "" {
		pps("path can't be empty: path: $path")
	}

	if !insert {
		rp.paths << path
	} else {
		rp.paths.insert(0, path)
	}
}

pub fn (mut rp ResPath) add_path(path string) {
	println("bits: ResPath add_path: path: $path")

	if rp.has_path(path) {
		pps("ResPaths already have path: path: $path")
	}

	rp.paths << path
}

pub fn (mut rp ResPath) add_paths(paths []string) {
	println("bits: ResPath add_paths: paths: $paths")
	for p in paths {
		rp.add_path(p)
	}
}

pub fn (mut rp ResPath) insert_path(path string) {
	println("bits: ResPath insert_path: path: $path")
	
	rp.do_add_path(path, true)
}

pub fn (mut rp ResPath) remove_path(path string) {
	println("bits: ResPath remove_path: path: $path")

	if !rp.has_path(path) {
		pps("no such path in ResPaths: path: $path")
	}

	idx := rp.paths.index(path)
	rp.paths.delete(idx)
}

pub fn (rp &ResPath) has_path(path string) bool {
	return path in rp.paths
}

// Return the topmost occurrence of path
//pub fn (rp &ResPath) resolve_path(path string) (bool, string) {
pub fn (rp &ResPath) resolve_path(path string) ?string {
	println("bits: ResPath ResolvePath: path: $path")

	mut ok := false

	mut resolved_path := ""
	mut found := false
	for rpath in rp.paths {
		xpath := rpath + "/" + path
		ok2 := os.exists(xpath)
		if ok2 {
			found = true
			resolved_path = xpath
			break
		}
	}

	if found {
		ok = true
	}

	println("ok: $ok")
	println("resolved_path: $resolved_path")

	if !ok {
		return error("res_path is not resolved: path: '$path'; res_paths: $rp.paths")
	}

	//return ok, resolved_path
	return resolved_path
}

pub fn (rp &ResPath) print_paths() {
	println("ResPaths:")
	for _, rpath in rp.paths {
		println("  " + rpath)
	}
	println("End ResPaths")
}
