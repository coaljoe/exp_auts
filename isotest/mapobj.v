module main

[heap]
struct MapObj {
mut:
	obj &Obj // Embed
	
	name string
	path string
	variant int

	/*

	// XXX pos?
	x int
	y int
	*/

	view &MapObjView
}

fn new_map_obj() &MapObj {
	mo := &MapObj{
		obj: new_obj(),
		view: &MapObjView(0),
	}

	return mo
}

fn (mut mo MapObj) place_at(cx int, cy int) {
	mo.obj.set_cx(cx)
	mo.obj.set_cy(cy)

	mut c := game.field.get_cell(cx, cy)
	c.map_obj = mo
	c.open = false
}

fn (mut mo MapObj) spawn() {
	println("mapobj spawn()")

	mo.view = new_map_obj_view(mo)
	mo.view.spawn()
}

fn (mut mo MapObj) update(dt f32) {

}
