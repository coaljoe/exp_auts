module main

import math

// TODO: add world_to_cell_pos, cell_to_world_pos (as an extra to screen versions)

// Cell pos ?
struct Pos {
mut:
	x int
	y int
}

// World pos to screen pos
fn world_to_screen_pos(x int, y int) (int, int) {

	sx := x + -game.viewport.shx
	sy := y + -game.viewport.shy

	return sx, sy
}

fn screen_to_world_pos(sx int, sy int) (int, int) {
	wx := sx + game.viewport.shx
	wy := sy + game.viewport.shy

	return wx, wy
}

// XXX returned value is top-left corner of cell
// use center version for cell's center
fn cell_to_world_pos(cx int, cy int) (int, int) {
	//wx := cx * c_cell_w
	//wy := cy * c_cell_h

	cell_size_z := 0
	cz := 0
	
	wx := (c_cell_w * cx) + ((cy&1) * (c_cell_w / 2))
	wy := (c_cell_h * cy / 2) - cell_size_z * cz

	return wx, wy
}

// XXX get center position of cell at cx, cy in world-space
fn cell_to_world_pos_center(cx int, cy int) (int, int) {
	wx, wy := cell_to_world_pos(cx, cy)

	center_wx := wx + c_cell_w / 2
	center_wy := wy + c_cell_h / 2

	return center_wx, center_wy
}

fn cell_to_screen_pos(cx int, cy int) (int, int) {
	pan_x := 0
	pan_y := 0
	cell_size_z := 0
	cz := 0
	
	sx := pan_x + (c_cell_w * cx) + ((cy&1) * (c_cell_w / 2))
	sy := pan_y + (c_cell_h * cy / 2) - cell_size_z * cz

	return sx, sy
}

// XXX bad
fn cell_to_screen_pos2(cx int, cy int) (int, int) {
			scale_ := f32(1.0)

			width_ := f32(c_cell_w)
			height_ := f32(c_cell_h)

			//width_ := f32(game.field.w * c_cell_w)
			//height_ := f32(game.field.h * c_cell_h)
			
            sw := scale_ * width_/2.0
            sh := scale_ * height_/2.0

            rx, ry := (cx - cy + f32(int(cy)&1)/2.0) * sw,
                      (cx + cy + f32(int(cy)&1)/2.0) * sh

            rxi, ryi := int(rx), int(ry)

            return rxi, ryi
}

/*
fn screen_to_cell_pos(sx, sy int) (int, int) {
	pan_x := 0
	pan_y := 0
	cell_size_z := 0

	mut cy := (2 * (sy - pan_y)) / c_cell_h
    mut cx := (sx - pan_x - ((cy&1) * (c_cell_w / 2))) / c_cell_w
    mut cz := 0

    // Correction?

    //mut xx, mut yy := cell_to_screen_pos(cx, cy, cz)
    mut xx, mut yy := cell_to_screen_pos(cx, cy)

    xx = sx - xx
    mx0 := cx
    yy = sy - yy
    my0 := cy
    
    if xx <= (c_cell_w / 2) {
    	if yy > xx * c_cell_h/c_cell_w {
    	    cy++
    	    if int(cy&1) != 0 {
    	    	cx--
  			}
    	}
    } else {
    	if yy > (c_cell_w - xx) * c_cell_h/c_cell_w {
    		cy++
    		if int(cy&1) == 0 {
    			cx++
    		}
    	}
    }

    return cx, cy
}
*/

// XXX bad
fn screen_to_cell_pos2(sx int, sy int) (int, int) {
	px := f32(sx)
	py := f32(sy)

	//width := game.field.w * c_cell_w
	//height := game.field.h * c_cell_h

	//width := game.field.w
	//height := game.field.h

	width := c_cell_w
	height := c_cell_h

    mx := int(math.floor(px / width))
    my := int(math.floor(py / height) * 2)

    return mx, my
}

fn screen_to_cell_pos3(sx int, sy int) (int, int) {
	wx, wy := screen_to_world_pos(sx, sy)
	return world_to_cell_pos3(wx, wy)
}

// XXX
// x is accurate +/-
// y is not very
//fn screen_to_cell_pos3(sx, sy int) (int, int) {
fn world_to_cell_pos3(wx int, wy int) (int, int) {

	// XXX name alias for world version
	sx := wx
	sy := wy

	//y := f32(game.field.h) - 1.0 - f32(sy) * 2.0 / f32(c_cell_h) 
	//sy_ := ctx.vars.res_y - sy
	//sy_ := game.field.height_px() - (f32(c_cell_h) * 1.5) - sy
	//sy_ := sy
	//sy_ := 1.0
	//sy_ := f32(0.0)
	//println("sy_: $sy_")
	//y := f32(game.field.h) - f32(sy_) * 2.0 / f32(c_cell_h) 
	//y := f32(game.field.h) - 1.0 - f32(sy_) * 2.0 / f32(c_cell_h) 
	//mut y := f32(sy) / f32(c_cell_h)
	//mut y := 0.0  - 1.0 - f32(sy_) * 2.0 / f32(c_cell_h) 
	mut y := f32(sy) * 2.0 / f32(c_cell_h) + 1.0
	//mut y := f32(sy) * 2.0 / f32(c_cell_h)
	mut x := f32(0.0)

	y = (2 * (sy)) / c_cell_h

	if int(y) % 2 == 0 {
		x = f32(sx) / f32(c_cell_w)
	} else {
		//x = f32(sx) / f32(c_cell_w)
		//x = (f32(sx) / f32(c_cell_w)) / 2.0
		//x = (f32(sx) + f32(c_cell_w / 2)) / f32(c_cell_w)
		x = (f32(sx) / f32(c_cell_w)) - f32(0.5) // 0.5 is float rounding correction to the lower number
	}

	yi, xi := int(y), int(x)

	return xi, yi
}

// XXX inaccurate
fn mouse_to_cell_pos(mx int, my int) (int, int) {
	// https://gamedev.stackexchange.com/questions/45103/staggered-isometric-map-calculate-map-coordinates-for-point-on-screen

	px := f32(mx) - (c_cell_w / 2.0)
	py := f32(my) - (c_cell_h / 2.0)

	x := math.floor((px + (py - (c_cell_h / 2.0)) * 2.0) / c_cell_w)
	y := math.floor((py - (px - (c_cell_w / 2.0)) * 0.5) / c_cell_h)

	
	tx := math.floor((x - y) / 2.0) + 1.0 //+ this.camera.x;
	ty := y + x + 2.0 //+ this.camera.y;
	

	/*
	tx := math.floor((x - y) / 2) + 1 + -(game.viewport.shx / c_tile_w)
	ty := y + x + 2 + -(game.viewport.shy / c_tile_h)
	*/

	// XXX rounding errors?
	mut txi := int(tx)
	mut tyi := int(ty)

	// XXX correct y by shift, fixme?
	tyi = tyi - 1

	/*
	vp_shx_tiles := game.viewport.shx / c_tile_w
	vp_shy_tiles := game.viewport.shy / c_tile_h

	txi = txi + vp_shx_tiles
	tyi = tyi + (vp_shy_tiles * 2)
	*/

	return txi, tyi
}

fn deg_between(x1 f32, y1 f32,x2 f32, y2 f32) f32 {
	return f32(math.degrees(math.atan2(y2-y1, x2-x1)))
}

/*
// XXX
//fn diag_line_points(sx int, sy int, ex int, ey int) [](int, int) {
//fn diag_line_points(sx int, sy int, ex int, ey int) (int, []int) {
//fn diag_line_points(sx int, sy int, ex int, ey int) [2]int {
fn diag_line_points(sx int, sy int, ex int, ey int) []Pos {
	println("diag_line_points: $sx, $sy, $ex, $ey")

	//ret := [](int, int)
	//ret := []int{}
	mut ret := []Pos{}

	mut adj_x := 0
	mut adj_y := 0

	if ey == sy {
		panic("not implemented")
	} else if ey > sy {
		adj_y = 1
	} else if ey < sy {
		adj_y = -1
	}

	if ex == sx {
		panic("not implemented")
	} else if ex > sx {
		adj_x = 1
	} else if ex < sx {
		adj_x = -1
	}

	println("adj_x: $adj_x")
	println("adj_y: $adj_y")

	if adj_x == 0 || adj_y == 0 {
		panic("bad")
	}

	// XXX
	mut px := sx
	mut py := sy

	for {
		p := &Pos{px, py}
		ret << p

		if px == ex && py == ey {
			break
		}

		px += adj_x
		py += adj_y
	}

	//len := ret.len

	println("ret: $ret")

	//return len, ret
	return ret
}
*/

// Bresenham's line, returns points/positions
fn line_points(x0_ int, y0_ int, x1 int, y1 int) []Pos {
	println("line_points: $x0_, $y0_, $x1, $y1")

	mut ret := []Pos{}

	// XXX
	mut x0 := x0_
	mut y0 := y0_

    mut dx := x1 - x0
    if dx < 0 {
        dx = -dx
    }
    mut dy := y1 - y0
    if dy < 0 {
        dy = -dy
    }
    //var sx, sy int
	mut sx := 0
	mut sy := 0
    if x0 < x1 {
        sx = 1
    } else {
        sx = -1
    }
    if y0 < y1 {
        sy = 1
    } else {
        sy = -1
    }
    mut err := dx - dy
 
    for {
        //b.SetPx(x0, y0, p)
		ret << Pos{x0, y0}

        if x0 == x1 && y0 == y1 {
            break
        }
        e2 := 2 * err
        if e2 > -dy {
            err -= dy
            x0 += sx
        }
        if e2 < dx {
            err += dx
            y0 += sy
        }
    }

	return ret
}

//fn diag_line_points(sx int, sy int, ex int, ey int) [](int, int) {
//fn diag_line_points(sx int, sy int, ex int, ey int) (int, []int) {
//fn diag_line_points(sx int, sy int, ex int, ey int) [2]int {
fn diag_line_points(sx int, sy int, ex int, ey int) []Pos {
	println("diag_line_points: $sx, $sy, $ex, $ey")

    // |  + diag point
	// | /:
	// |/ e (end point)
	// s--:

	//ret := [](int, int)
	//ret := []int{}
	mut ret := []Pos{}

	// XXX special cases
	// same point
	if sx == ex && sy == ey {
		println("same point")
		// XXX return one point
		ret << Pos{sx, sy}
		return ret
	}
	// parallel lines
	if (sy == ey) || (sx == ex) {
		println("the line is parallel")
		// Only one segment
		ret = line_points(sx, sy, ex, ey)
		println("ret: $ret")
		return ret
	}

	// Find diag point

	diag_x := ex // side sx-ex
	diag_y := ex // XXX 90 degree square/triangle, same as length x

	println("diag_x: $diag_x")
	println("diag_y: $diag_y")

	// Diag point y shift/diff
	//diag_yshift := ey - diag_y
	//diag_yshift := math.abs(ey) - math.abs(diag_y)
	diag_yshift := ey - diag_y
	shift_length := math.abs(diag_yshift)
	//shift_length := math.abs(ey) - math.abs(diag_y)

	println("diag_yshift: $diag_yshift")
	println("shift_length: $shift_length")

	mut p1x := 0
	mut p1y := 0

	// Diagonal line
	if diag_yshift == 0 {
		//panic("not implemented")
		println("the line is diagonal")
		// Only one segment
		ret = line_points(sx, sy, ex, ey)
		println("ret: $ret")
		return ret
	} else if diag_yshift > 0 {
		//p1y = shift_length
		p1y = sy + shift_length
	} else if diag_yshift < 0 {
		//p1x = shift_length
		p1x = sx + shift_length
	}

	println("pre p1: $p1x, $p1y")

	// Do some p1 wrapping / inversions
	// for negative coords
	if ey < 0 && ex > 0 {
		println("wrap y")
		p1y = -p1y
	}
	if ex < 0 && ey > 0 {
		println("wrap x")
		p1x = -p1x
	}
	if ex < 0 && ey < 0 {
		println("wrap xy")
		p1x, p1y = -p1y, -p1x
	}

	println("p1: $p1x, $p1y")

	// Draw first segment, from start to p1

	//segment1 := line_points(sx, sy, p1x, p1y)
	
	// hline XXX
	segment1 := line_points(sx, sy, p1x, sx)

	println("segment1: $segment1")

	ret << segment1


	// Draw second segment, from p1 to end

	mut segment2 := line_points(p1x, p1y, ex, ey)

	// XXX pop first point, therefore no duplicates exist
	segment2.delete(0)

	println("segment2: $segment2")

	ret << segment2

	//pp(2)

	println("ret: $ret")

	return ret
}
